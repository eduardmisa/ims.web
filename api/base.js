import { MAINTENANCE, TRANSACTIONS } from '@/api/urls.js'
import User from '@/api/modules/user'
import Brand from '@/api/modules/brand'
import Category from '@/api/modules/category'
import Currency from '@/api/modules/currency'
import ItemCondition from '@/api/modules/item_condition'
import News from '@/api/modules/news'
import UnitMaterial from '@/api/modules/unit_material'
import Client from '@/api/modules/client'
import Project from '@/api/modules/project'
import ProjectLocation from '@/api/modules/project_location'
import Warehouse from '@/api/modules/warehouse'
import Supplier from '@/api/modules/supplier'

import Product from '@/api/modules/product'
import Inventory from '@/api/modules/inventory'

import Acquisition from '@/api/modules/acquisition'

import Requisition from '@/api/modules/requisition'


export const apiFactory = axios => ({
    User: new User(axios, MAINTENANCE.User),
    Brand: new Brand(axios, MAINTENANCE.Brand),
    Category: new Category(axios, MAINTENANCE.Category),
    Currency: new Currency(axios, MAINTENANCE.Currency),
    ItemCondition: new ItemCondition(axios, MAINTENANCE.ItemCondition),
    News: new News(axios, MAINTENANCE.Announcement),
    UnitMaterial: new UnitMaterial(axios, MAINTENANCE.UnitMaterial),
    Client: new Client(axios, MAINTENANCE.Client),
    Project: new Project(axios, MAINTENANCE.Project),
    ProjectLocation: new ProjectLocation(axios, MAINTENANCE.ProjectLocation),
    Warehouse: new Warehouse(axios, MAINTENANCE.Warehouse),
    Supplier: new Supplier(axios, MAINTENANCE.Supplier),

    Product: new Product(axios, MAINTENANCE.Product),
    Inventory: new Inventory(axios, TRANSACTIONS.Inventory),
    Acquisition: new Acquisition(axios, TRANSACTIONS.AcquisitionStock),

    Requisition: new Requisition(axios, TRANSACTIONS.RequisitionStock),
  })