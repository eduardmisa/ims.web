const consoleLogger = require('../utils/logger').ConsoleLogger
const fileLogger = require('../utils/logger').FileLogger

// const axios = require('axios') // TODO: Should Use @nuxt/axios

const list = require('../helpers/listHelper').default

const { axios_frontend_host, axios_frontend_port } = require('../nuxt.env.json')

var loadingCallback = null

var promiseArray = []
const promiseHelper = (promiseObject) => { 

    if (process.server) return // We don't need to track promises in Nuxt Server side

    let existing = promiseArray.find(promiseItem => promiseItem === promiseObject)

    ! existing ? promiseArray.push(promiseObject) : list.delete(promiseArray, { key: promiseObject })

    promiseArray.length > 0 ? loadingCallback(true) : loadingCallback(false)

    // consoleLogger(`PROMISE ARRAY:`, promiseArray);
}

const slugHelper = (url, slug) => {

    if (slug) {

        slug = String(slug)
        
        if (!url.endsWith('/')) 
          url += '/'
  
        if (slug.startsWith('/'))
          slug = slug.substring(1, slug.length)
        
        url += slug
        
        if (!slug.endsWith('/')) 
          url += '/'
      }
  
      return url
}
const paramsHelper = (url, params) => {

    // params = {key: '', value: ''}

    if (params) {

        if (!url.endsWith('/')) 
          url += '/'
  
        url += '?'
  
        params.forEach(paramObj => {
          url += paramObj.key + '=' + paramObj.value + '&'
        });
  
        // Remove Trailing '&'
        url = url.substring(0, url.length - 1)
      }
  
      return url
}
const urlBuilder = (url) => { return `${axios_frontend_host}:${axios_frontend_port}/${url}` }

const urlHelper = ({url, slug, params}) => { return urlBuilder(paramsHelper(slugHelper(url, slug), params)) }


/*
  You can chain the returned promise.
  Callbacks will be called first before your chains.
*/
export default class Request {
  constructor (axios, baseUrl) {
    this.axios = axios
    this.baseUrl = baseUrl
  }

  init({loadingCB}) {

    loadingCallback = loadingCB
  }

  get_request ({slug, params, success, fail}) {

      consoleLogger(`PROCESS OWNER:`, process.server ? 'server' : 'client');

      let furl = urlHelper({url: this.baseUrl, slug, params})

      consoleLogger(`GET[${furl}]: ...`)

      let promise = this.axios.get(furl)
        .then(function({data}) { /*promiseHelper(promise);*/ consoleLogger(`GOT[${furl}]: ${data}`);
            if (typeof success === 'function') success(data)
        })
        .catch(function(error) { /*promiseHelper(promise);*/ consoleLogger(`FAIL[${furl}]: ${error}`);
            if (typeof fail === 'function') fail(error)            
        })

      /*promiseHelper(promise);*/

      return promise
  }

  post_request ({slug, form, success, fail}) {

    consoleLogger(`PROCESS OWNER:`, process.server ? 'server' : 'client');

    let furl = urlHelper({url: this.baseUrl, slug})

    consoleLogger(`POST[${furl}]: ...`)

    let promise = this.axios.post(furl, form)
      .then(function({data}) { promiseHelper(promise); consoleLogger(`POSTED[${furl}]: ${data}`);
        
          if (typeof success === 'function') success(data)
      })
      .catch(function(error) { promiseHelper(promise); consoleLogger(`FAIL[${furl}]: ${error}`);
          
          if (typeof fail === 'function') fail(error)            
      })

    promiseHelper(promise)

    return promise

  }

  put_request ({slug, form, success, fail}) {

    consoleLogger(`PROCESS OWNER:`, process.server ? 'server' : 'client');

    let furl = urlHelper({url: this.baseUrl, slug})

    consoleLogger(`PUT[${furl}]: ...`)

    let promise = this.axios.put(furl, form)
      .then(function({data}) { promiseHelper(promise); consoleLogger(`PUTTED[${furl}]: ${data}`);
        
          if (typeof success === 'function') success(data)
      })
      .catch(function(error) { promiseHelper(promise); consoleLogger(`FAIL[${furl}]: ${error}`);
          
          if (typeof fail === 'function') fail(error)            
      })

    promiseHelper(promise)

    return promise


  }

  patch_request ({slug, form, success, fail}) {

    consoleLogger(`PROCESS OWNER:`, process.server ? 'server' : 'client');

    let furl = urlHelper({url: this.baseUrl, slug})

    consoleLogger(`PATCH[${furl}]: ...`)

    let promise = this.axios.patch(furl, form)
      .then(function({data}) { promiseHelper(promise); consoleLogger(`PACTHED[${furl}]: ${data}`);
        
          if (typeof success === 'function') success(data)
      })
      .catch(function(error) { promiseHelper(promise); consoleLogger(`FAIL[${furl}]: ${error}`);
          
          if (typeof fail === 'function') fail(error)            
      })

    promiseHelper(promise)

    return promise

  }

  delete_request ({slug, success, fail}) {

    consoleLogger(`PROCESS OWNER:`, process.server ? 'server' : 'client');

    let furl = urlHelper({url: this.baseUrl, slug})

    consoleLogger(`PATCH[${furl}]: ...`)

    let promise = this.axios.delete(furl)
      .then(function({data}) { promiseHelper(promise); consoleLogger(`DELETED[${furl}]: ${data}`);
        
          if (typeof success === 'function') success(data)
      })
      .catch(function(error) { promiseHelper(promise); consoleLogger(`FAIL[${furl}]: ${error}`);
          
          if (typeof fail === 'function') fail(error)            
      })

    promiseHelper(promise)

    return promise

  }
}