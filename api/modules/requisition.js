import Request from '@/api/request'

export default class Requisition extends Request {
    constructor (axios, url) {
        super(axios, url)
    }

    get ({slug, params, success, fail}) {
        return this.get_request({slug, params, success, fail})
    }
  
    post ({slug, form, success, fail}) {
        return this.post_request({slug, form, success, fail})
    }
  
    put ({slug, form, success, fail}) {
        return this.put_request({slug, form, success, fail})
    }
  
    patch ({slug, form, success, fail}) {
        return this.patch_request({slug, form, success, fail})
    }
  
    delete ({slug, success, fail}) {
        return this.delete_request({slug, success, fail})
    }

    paginate ({params, success, fail}) {
        return this.get_request({params, success, fail})
    }

    listStocksForRequisition ({params, success, fail}) {
        return this.get_request({slug: '/stocks-for-requisition/', params, success, fail})
    }


    listStocksRequisitionItems ({slug, params, success, fail}) {
        return this.get_request({slug: `/${slug}/stock-request-items/`, params, success, fail})
    }

    listPurchaseRequestItems ({slug, params, success, fail}) {
        return this.get_request({slug: `/${slug}/purchase-request-items/`, params, success, fail})
    }

    // TODO: IMPLEMENT THIS
    listCheckoutsItems ({slug, params, success, fail}) {
        return this.get_request({slug: `/${slug}/checked-out-items/`, params, success, fail})
    }


    listStocksRequisitionItemsForApproval ({slug, params, success, fail}) {
        return this.get_request({slug: `/${slug}/approve-stock-request-items/`, params, success, fail})
    }

    listPurchaseRequestItemsForApproval ({slug, params, success, fail}) {
        return this.get_request({slug: `/${slug}/approve-purchase-request-items/`, params, success, fail})
    }

    approve ({slug, form, success, fail}) {
        return this.put_request({slug: `${slug}/approve/`, form, success, fail})
    }

    reject ({slug, form, success, fail}) {
        return this.put_request({slug: `${slug}/reject/`, form, success, fail})
    }
}