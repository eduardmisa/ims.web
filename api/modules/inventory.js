import Request from '@/api/request'

export default class Inventory extends Request {
    constructor (axios, url) {
        super(axios, url)
    }

    get ({slug, params, success, fail}) {
        return this.get_request({slug, params, success, fail})
    }
  
    post ({slug, form, success, fail}) {
        return this.post_request({slug, form, success, fail})
    }
  
    put ({slug, form, success, fail}) {
        return this.put_request({slug, form, success, fail})
    }
  
    patch ({slug, form, success, fail}) {
        return this.patch_request({slug, form, success, fail})
    }
  
    delete ({slug, success, fail}) {
        return this.delete_request({slug, success, fail})
    }

    paginate ({params, success, fail}) {
        return this.get_request({params, success, fail})
    }

    listInStoreLocation ({slug, params, success, fail}) {
        return this.get_request({slug: `${slug}/instore-location/`, params, success, fail})
    }
    listInStoreLocationBatch({slug, params, success, fail}) {
        return this.get_request({slug: `${slug}/instore-location-batch/`, params, success, fail})
    }
    listOut ({slug, params, success, fail}) {
        return this.get_request({slug: `${slug}/checkout-items/`, params, success, fail})
    }
    listReturns ({slug, params, success, fail}) {
        return this.get_request({slug: `${slug}/return-items/`, params, success, fail})
    }
    listBatchHistory ({slug, params, success, fail}) {
        return this.get_request({slug: `${slug}/batch-history/`, params, success, fail})
    }
}