const NO_AUTH_ROUTES = [
  'login'
]

export default function ({ store, route, redirect  }) {
    // If the user is not authenticated
    if (!store.state.auth.loggedIn) {
      redirect('/login')
    }
    // else {
    //   if (NO_AUTH_ROUTES.includes(route.name)) {
    //     redirect('/')
    //   }
    // }
  }