export const Table = class Table {
    constructor (headers) {
        this.headers = headers
        this.data = [] 
        this.total = 0,
        this.loading = false,
        this.search = { field: '', value: '', fields: {} },
        this.pagination = {
          descending: false,
          page: 1,
          rowsPerPage: -1, // -1 for All
          sortBy: null,
          totalItems: 0,
        },

        // HELPER METHODS
        this.additionalFilters = []
    }

    toPOJO () {
      return {
        headers: this.headers,
        data: this.data,
        total: this.total,
        loading: this.loading,
        search: this.search,
        pagination: this.pagination,

        additionalFilters: this.additionalFilters
      }
    }
}