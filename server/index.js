const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express()

const authJWTRouter = require('./auth/token/authRouter')
const router = require('./router')

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  app.use(express.json())
  app.use('/api', router)
  app.use('/auth', authJWTRouter)
  // Give nuxt middleware to express
  // This should be at last '.use'
  app.use(nuxt.render) 

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on EDUARDMISA http://${host}:${port}`,
    badge: true
  })
}
start()
