const consoleLogger = require('./utils/logger').ConsoleLogger

const axios = require('axios')
const appAxios = axios.create({
  baseURL: `${require('../nuxt.env.json').axios_express_host}:${require('../nuxt.env.json').axios_express_port}/`
})

module.exports = {
    get: ({req, res, url, successCallback, errorCallback}) => {

        let furl = `${appAxios.defaults.baseURL}${url}`

        consoleLogger(`(EXPRESS) GETTING[${furl}]:`, '...')
    
        var headers = Object.assign({}, { "authorization": req.headers.authorization })
  
        appAxios.get(url, { headers })
            .then(function (response) { 
                consoleLogger(`(EXPRESS) GOT[${furl}]:`, response.data)
                res.status(response.status)
                successCallback(response.data)
            })
            .catch(function(error) { 
                consoleLogger(`(EXPRESS) FAIL[${furl}]:`, error.message)
                res.status(error.response.status)
                errorCallback(error)
            })
    },
    post: ({req, res, url, successCallback, errorCallback}) => {

        let furl = `${appAxios.defaults.baseURL}${url}`

        consoleLogger(`(EXPRESS) POSTING[${furl}]:`, '...')
  
        var headers = Object.assign({}, { "authorization": req.headers.authorization })
    
        appAxios.post(url, req.body, headers)
            .then(function (response) { 
                consoleLogger(`(EXPRESS) POSTED[${furl}]:`, response.data)
                res.status(response.status)
                successCallback(response.data)
            })
            .catch(function(error) { 
                consoleLogger(`(EXPRESS) FAIL[${furl}]:`, error.message)
                res.status(error.response.status)
                errorCallback(error)
            })
    }
  }