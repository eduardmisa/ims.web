const { Router } = require('./node_modules/express')
const router = Router()

const appAxios = require('../../axiosHelper.js')

router.post('/login/', function (req, res, next) {
    appAxios.post({req, res, 
        url: 'login/', 
        successCallback: (data) => {
          res.json(data)
        },
        errorCallback: (error) => {
          res.json(error.message)
        }
      })
})

router.get('/current-user/', function (req, res, next) {
    appAxios.get({req, res, 
        url: 'current-user', 
        successCallback: (data) => {
          res.json(data)
        },
        errorCallback: (error) => {
          res.json(error.message)
        }
      })
})

router.post('/forgot-password/', function (req, res, next) {

})

module.exports = router