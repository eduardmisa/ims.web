const { Router } = require('express')
const router = Router()

const appAxios = require('../../axiosHelper.js')

router.post('/login/', function (req, res, next) {
    appAxios.post({req, res, 
        url: 'api/v1/auth/login/', 
        successCallback: (data) => {
          res.json(data)
        },
        errorCallback: (error) => {
          res.json(error.message)
        }
      })
})

router.get('/current-user/', function (req, res, next) {
    appAxios.get({req, res, 
        url: 'api/v1/auth/current-user/', 
        successCallback: (data) => {
          res.json(data)
        },
        errorCallback: (error) => {
          res.json(error.message)
        }
      })
})

router.post('/logout/', function (req, res, next) {
  appAxios.post({req, res, 
      url: 'api/v1/auth/logout/', 
      successCallback: (data) => {
        res.json(data)
      },
      errorCallback: (error) => {
        res.json(error.message)
      }
    })
})

router.post('/forgot-password/', function (req, res, next) {

})

module.exports = router