const { Router } = require('express')
const router = Router()

const appAxios = require('./axiosHelper.js')


// **************************** ENCRYPTION **************************** //
const crypto = require('crypto');

const encryption_key = 'TEST_KEY'

const algorithm = 'des-ede3-cbc'

function cipher (plaintext) {
  var cipher = crypto.createCipheriv(algorithm, encryption_key, iv)
  var ciph = cipher.update(plaintext, 'utf8', 'base64')
  ciph += cipher.final('base64')
}

function decipher (ciph) {
  var decipher = crypto.createDecipheriv(algorithm, encryption_key, iv)
  var txt = decipher.update(ciph, 'base64', 'utf8')
  txt += decipher.final('utf8')
}
// **************************** ENCRYPTION **************************** //


// Mock Users
const users = require('../json/users.json')
const products = require('../json/products.json')




/* ***************** MOCK DATA ***************** */

/* GET users listing. */
router.get('/users', function (req, res, next) {

  // Sorting
  let sortField = req.query['sort-field']
  let sortOrder = req.query['sort-order']

  if (sortField && sortOrder) {

    switch (sortOrder.toLowerCase()) {

      case 'asc':
        var oo = 10
      break;

      case 'desc':
        var oo = 20
      break;
    }
  }

  // Paging
  let page = req.query['page'] ? (Number(req.query['page'])) : 0
  let pageSize = req.query['page-size'] ? (Number(req.query['page-size'])) : 0

  let pageStart = (page - 1) * pageSize
  let pageEnd = pageStart + pageSize

  let data = users.slice(pageStart, pageEnd)
  
  setTimeout(() => {
    res.json({
      results: data,
      page: page,
      count: users.length
    })
  }, 100)


})


router.get('/products', function (req, res, next) {

  // Sorting
  let sortField = req.query['sort-field']
  let sortOrder = req.query['sort-order']

  if (sortField && sortOrder) {

    switch (sortOrder.toLowerCase()) {

      case 'asc':
        var oo = 10
      break;

      case 'desc':
        var oo = 20
      break;
    }
  }

  // Paging
  let page = req.query['page'] ? (Number(req.query['page'])) : 0
  let pageSize = req.query['page-size'] ? (Number(req.query['page-size'])) : 0

  let pageStart = (page - 1) * pageSize
  let pageEnd = pageStart + pageSize

  let data = products.slice(pageStart, pageEnd)

  setTimeout(() => {
    res.json({
      results: data,
      page: page,
      count: products.length
    })
  }, 100)
})






/* GET user by ID. */
router.get('/users/:id', function (req, res, next) {
  const id = parseInt(req.params.id)
  if (id >= 0 && id < users.length) {
    res.json(users[id])
  } else {
    res.sendStatus(404)
  }
})


// router.all('*', function (req, res, next) {

//   if (req.method && req.method.toLowerCase() === 'post') {

//     // Decrypt Headers Here

//     // const id = parseInt(req.params.id)
//     // if (id >= 0 && id < users.length) {
//     //   res.json(users[id])
//     // } else {
//     //   res.sendStatus(404)
//     // }
//   }
//   else 
//     res.status(405).json({"AMS": "Not Allowed"})
// })

module.exports = router