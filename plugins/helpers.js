import tableHelper from '@/helpers/tableHelper.js'

export default (context, inject) => {

    const helperFactory = {
        vuetifyTable: tableHelper,
    }

    inject('helpers', helperFactory)
}
// import Vue from 'vue'
// import product from '@/plugins/datalayer/product'

// Vue.mixin({
//     data() {
//         return {
//             // moment: moment
//         }
//     },
//     computed: {
//         businesslayer() {
//             return {
//                 product: 'TEST GAGO'
//             }
//         }
//     }
// })