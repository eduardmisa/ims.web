import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'
import config_helper from '@/helpers/config_helper.js'

Vue.use(VueGoogleMaps, {
  load: {
    key: config_helper.GetValue('map_API_KEY'),
    libraries: 'places', 
  },
})