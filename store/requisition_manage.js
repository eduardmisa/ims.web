import { requisition_manage } from '@/store-schema/requisition_manage'

export const state = () => ({
  viewmodel: new requisition_manage().toPOJO()
})



export const getters = {
  getIndexCheckoutOpenTable: state => {
    return state.viewmodel.index.checkoutOpen.table
  },
  getIndexCheckoutCloseTable: state => {
    return state.viewmodel.index.checkoutClose.table
  }
}




export const mutations = {
  reset (state) {
    Object.assign(state.viewmodel, new requisition_manage().toPOJO())
  },
  fillCheckoutOpen (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.index.checkoutOpen.table, data)
  },
  fillCheckoutClose (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.index.checkoutClose.table, data)
  }
}



export const actions = {
  reset (state) {
    state.commit('reset')
  },
  async listCheckoutOpen(state) {
    let tbl = state.getters.getIndexCheckoutOpenTable
    let params = this.$helpers.vuetifyTable.getParamsVuetify(tbl)

    params.push({key: 'checkout_close', value: false })

    return await this.$api.Requisition.paginate({
        params,
        success: (data) => {
          state.commit('fillCheckoutOpen', data)
        },
        fail: (error) => {
          tbl.loading = false
          tbl.data = []
        }
    })
  },

  async listCheckoutClose(state){
    let tbl = state.getters.getIndexCheckoutCloseTable
    let params = this.$helpers.vuetifyTable.getParamsVuetify(tbl)

    params.push({key: 'checkout_close', value: true })

    return await this.$api.Requisition.paginate({
        params,
        success: (data) => {
          state.commit('fillCheckoutClose', data)
        },
        fail: (error) => {
          tbl.loading = false
          tbl.data = []
        }
    })
  },
}