import objectHelper  from '@/helpers/object_helper.js'
import { requisition_for_approval } from '@/store-schema/requisition_for_approval'

export const state = () => ({
  viewmodel: new requisition_for_approval().toPOJO()
})



export const getters = {
  // getProduct: state => {
  //   return state.viewmodel.product
  // }
  getTable: state => {
    return state.viewmodel.index.table
  },
}



export const mutations = {
  reset (state) {
    Object.assign(state.viewmodel, new requisition_for_approval().toPOJO())
  },
  fillTable (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.index.table, data)
  },
}



export const actions = {
  reset (state) {
    state.commit('reset')
  },
  list (state) {

    var tbl = state.getters.getTable

    let params = this.$helpers.vuetifyTable.getParamsVuetify(state.getters.getTable)
    params.push({
      key:'approver',
      value: this.$auth.user.id
    })

    return this.$api.Requisition.paginate({
        params,
        success: (data) => {
          state.commit('fillTable', data)
        },
        fail: (error) => {
          console.log('Master error [list] @'+ state.getters.getModule)
          tbl.loading = false
          tbl.data = []
        }
    })
  },
}
