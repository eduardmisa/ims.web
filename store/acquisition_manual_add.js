const { Table } = require('../classes/table')
import { ManualAdd } from '@/store-schema/acquisition_manual_add'
import helperObject from '@/helpers/object_helper.js'

export const state = () => ({
  viewmodel: new ManualAdd().toPOJO()
})


export const getters = {
  getProduct: state => {
    return state.viewmodel.currentProduct
  },
  getForm: state => {
    return state.viewmodel.form
  },
  getFormTemp: state => {
    return state.viewmodel.form_temp
  },
  getStocksLength: state => {
    return state.viewmodel.form.stocks.data.length
  },
  // CONTROLS
  enableStep1: state => {
    return state.viewmodel.form.acquired_date != null
      && state.viewmodel.form.warehouse > 0
      && state.viewmodel.form.supplier > 0
  },
  enableStep3: state => {
    return state.viewmodel.form.stocks.data.length > 0
  }
}


export const mutations = {
  reset (state) {
    Object.assign(state.viewmodel, new ManualAdd().toPOJO())
  },
  setProduct (state, product) {
    state.viewmodel.currentProduct = product
  },
  addSerializedStock (state) {

    state.viewmodel.form.stocks.data = []

    let product_id = state.viewmodel.currentProduct.prod_id    
    let serials = state.viewmodel.form_temp.serials

    let batch_price = state.viewmodel.form_temp.total_price
    let batch_currency = state.viewmodel.form_temp.currency
    let batch_item_condition = state.viewmodel.form_temp.item_condition

    let each_price = batch_price / serials.length

    serials.forEach((serial) => {
        state.viewmodel.form.stocks.data.push({
            serial_number: serial,
            product: product_id,
            acquisition_items: [{
                quantity: 1,
                price: each_price,
                currency: batch_currency,
                item_condition: batch_item_condition,
            }]
        })
    })

    state.viewmodel.form.batch_price = state.viewmodel.form_temp.total_price

    state.viewmodel.form_temp.serials = []
    state.viewmodel.form_temp.total_price = 0
    state.viewmodel.form_temp.currency = null
    state.viewmodel.form_temp.item_condition = null
  },
  updateSerializedStock (state) {

    state.viewmodel.form_temp.serials = []

    let currency = null
    let item_condition = null
    let each_price = 0
    let stocks = state.viewmodel.form.stocks.data
    
    stocks.forEach((stock) => {

        state.viewmodel.form_temp.serials.push(stock.serial_number)
        each_price = stock.acquisition_items[0].price
        currency = stock.acquisition_items[0].currency
        item_condition = stock.acquisition_items[0].item_condition
    })

    state.viewmodel.form_temp.total_price = stocks.length * each_price
    state.viewmodel.form_temp.currency = currency
    state.viewmodel.form_temp.item_condition = item_condition
    state.viewmodel.form.stocks.data = []
  },
  addConsumableStock (state) {

    state.viewmodel.form.stocks.data = []

    let product_id = state.viewmodel.currentProduct.prod_id

    let batch_price = state.viewmodel.form_temp.total_price
    let batch_currency = state.viewmodel.form_temp.currency
    let batch_item_condition = state.viewmodel.form_temp.item_condition
    let batch_quantity = state.viewmodel.form_temp.total_quantity

    let each_price = batch_price / batch_quantity

    state.viewmodel.form.stocks.data.push({
        serial_number: null,
        product: product_id,
        acquisition_items: [{
            quantity: batch_quantity,
            price: each_price,
            currency: batch_currency,
            item_condition: batch_item_condition,
        }]
    })

    state.viewmodel.form.batch_price = state.viewmodel.form_temp.total_price
  },
  updateConsumableStock (state) {
    state.viewmodel.form.stocks.data = []
  }
}


export const actions = {
  reset (state) {
    state.commit('reset')
  },
  async fetchProduct (state, id) {
    await this.$api.Inventory.get({
      slug: id,
      success: (data) => {  
        state.commit('setProduct', data)
      },
      fail: () => {
        
      }
    })
  },
  async SUBMIT_FORM (state) {
    
    var data = state.getters.getForm

    // state.dispatch('removeErrors')

    let form = state.getters.getForm
    
    let formData = {}
    for (var key in form) {
      formData[key] = form[key]
    }

    formData.stocks = form.stocks.data

    this.$api.Acquisition.post({
      form: formData,
      success: () => {
        state.dispatch('common/toggleConfirmManual', 
        {
          title: 'Manual Add',
          message: 'Batch successfully added!',
          possitiveAction: () => {
            state.dispatch('common/toggleConfirmManual', false, {root:true})
            this.$router.replace(`/transaction/inventory/${state.getters.getProduct.prod_id}`)
          },
          possitiveActionText: 'Back to Inventory',
        }
        ,{root:true})
      },
      fail: (error) => {

        try {

          var errorMessageBody = error.response.data
  
          var str = JSON.stringify(errorMessageBody.error);
          
          str = str.replace(/batch_no/g, 'batch_no_error');
          str = str.replace(/acquired_date/g, 'acquired_date_error');
          str = str.replace(/po_no/g, 'po_no_error');
          str = str.replace(/inv_no/g, 'inv_no_error');
          str = str.replace(/serial_number/g, 'serial_number_error');
          str = str.replace(/product/g, 'product_error');
          str = str.replace(/price/g, 'price_error');
          str = str.replace(/total_quantity/g, 'total_quantity_error');
          str = str.replace(/warehouse/g, 'warehouse_error');
          str = str.replace(/supplier/g, 'supplier_error');
          str = str.replace(/condition/g, 'condition_error');
          str = str.replace(/currency/g, 'currency_error');
 
          let lazyObject = Object.assign({}, JSON.parse(str));
          
          if (lazyObject && lazyObject.stocks && lazyObject.stocks.length > 0) {
            Object.assign(lazyObject, 
              { 
                stocks: 
                {
                  data: lazyObject.stocks
                }
              }
            )
          }
            
          data = helperObject.merge(data, lazyObject)
          state.dispatch('common/toggleSnackbar', `Form failed to be submited!`, {root:true})
        }
        catch {
          state.dispatch('common/toggleSnackbar', `Form failed to be submited!\n${String(error)}`, {root:true})
        }
      }
    })
  },
}
