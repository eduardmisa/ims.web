import objectHelper  from '@/helpers/object_helper.js'
import { requisition_create } from '@/store-schema/requisition_create'

export const state = () => ({
  viewmodel: new requisition_create().toPOJO()
})



export const getters = {
  getProduct: state => {
    return state.viewmodel.product
  },
  getStep1: state => {
    return state.viewmodel.step1
  },
  getStep2: state => {
    return state.viewmodel.step2
  },
}



export const mutations = {
  reset (state) {
    Object.assign(state.viewmodel, new requisition_create().toPOJO())
  },
  toggleModal (state, key) {
    state.viewmodel.modals[key] = !state.viewmodel.modals[key]
  },
  fillStocksForRequisition (state, data) {

    if (data && data.results.length > 0) {

      data.results.forEach((item) => {

        var existing = state.viewmodel.step2.selectedItems.table.data.find(exItem => exItem.product == item.product)

        if (!existing) {
          //Set VB
          // item.qty_vb = item.qty_total - item.qty_used
          item.qty_add = 1
          item.qty_minus = 1
          item.qty_pr = 0
          item.qty_sr = 0
        }
        else {
          Object.assign(item, existing)
        }
      });
    }

    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.step2.selectionItems.table, data)
  },
  step2PlusSR (state, obj) {
    if (obj) {
      if (obj.qty_add > obj.qty_vb) {
        obj.qty_pr += obj.qty_add -  obj.qty_vb
        obj.qty_add = obj.qty_vb;
      }
  
      var add = parseInt(obj.qty_add)
  
      obj.qty_sr += add
  
      obj.qty_vb -= add
  
      obj.qty_add = 1
  
  
      var existing = state.viewmodel.step2.selectedItems.table.data.find(item => item.product == obj.product)
  
      if (!existing) 
        state.viewmodel.step2.selectedItems.table.data.push(obj)
      else {
        existing.qty_sr = obj.qty_sr
        existing.qty_vb = obj.qty_vb
      }
    }
  },
  step2PlusPR (state, obj) {
    if (obj) {
      var add = parseInt(obj.qty_add)

      obj.qty_pr += add
  
      obj.qty_add = 1
  
      var existing = state.viewmodel.step2.selectedItems.table.data.find(item => item.product == obj.product)
  
      if (!existing) 
        state.viewmodel.step2.selectedItems.table.data.push(obj)
      else {
        existing.qty_pr = obj.qty_pr
        existing.qty_vb = obj.qty_vb
      }
    }
  },
  step2MinusSR (state, obj) {
    if (obj) {
      var minus = parseInt(obj.qty_minus)

      var existing = state.viewmodel.step2.selectionItems.table.data.find(item => item.product == obj.product)
  
      if (existing) {
  
        if (obj.qty_sr - minus < 0) 
          minus = obj.qty_sr 
        
        existing.qty_sr -= minus
        existing.qty_vb += minus
  
        Object.assign(obj, existing)
  
        obj.qty_minus = 1
      }
  
       
      if (obj.qty_sr == 0 && obj.qty_pr == 0) {
        state.viewmodel.step2.selectedItems.table.data.splice(state.viewmodel.step2.selectedItems.table.data.indexOf(obj), 1)
      }
    }


  },
  step2MinusPR (state, obj) {
    if (obj) {
      var existing = state.viewmodel.step2.selectionItems.table.data.find(item => item.product == obj.product)

      if (existing) {
  
        var minus = parseInt(obj.qty_minus)
  
        if (obj.qty_pr - minus < 0) 
          minus = obj.qty_pr 
    
        existing.qty_pr -= minus
  
        Object.assign(obj, existing)
        
        obj.qty_minus = 1
      }
  
      if (obj.qty_sr == 0 && obj.qty_pr == 0) {
        state.viewmodel.step2.selectedItems.table.data.splice(state.viewmodel.step2.selectedItems.table.data.indexOf(obj), 1)
      }
    }
  },
  clearSelection (state) {
    state.viewmodel.step2.selectionItems.table.data.forEach((item) => {
      
      item.qty_vb = item.qty_total - item.qty_pri > 0 ? item.qty_total - item.qty_pri : 0
      item.qty_add = 1
      item.qty_minus = 1
      item.qty_pr = 0
      item.qty_sr = 0
    })

    state.viewmodel.step2.selectedItems.table.data = []
  },
}



export const actions = {
  reset (state) {
    state.commit('reset')
  },
  async step2PopulateStocksForRequisition (state) {
    await this.$api.Requisition.listStocksForRequisition({
      params: this.$helpers.vuetifyTable.getParamsVuetify(state.getters.getStep2.selectionItems.table),
      success: (data) => {
        state.commit('fillStocksForRequisition', data)
      }
    })
  },
  step1ShowAllProjects (state) {
    if (state.getters.getStep1.showAllProjects)
      state.dispatch('maintenance/populateProjects', {}, {root: true})
  },
  step1FilterProjects (state) {
    state.dispatch('maintenance/populateProjects', { params: [{key:'client', value: state.getters.getStep1.client}] }, {root: true})
  },
  step1SelectedProject (state) {

    let step1 = state.getters.getStep1

    this.$api.Project.get({
      slug: step1.project,
      success: (data) => {
        step1.projectDescription = data.description
      }
    })
  },
  step2PlusSR (state, obj) {
    state.commit('step2PlusSR', obj)
  },
  step2PlusPR (state, obj) {
    state.commit('step2PlusPR', obj)
  },
  step2MinusSR (state, obj) {
    state.commit('step2MinusSR', obj)
  },
  step2MinusPR (state, obj) {
    state.commit('step2MinusPR', obj)
  },
  clearSelection (state) {
    state.commit('clearSelection')
  },
  confirmSubmit (state) {

    var step1 = state.getters.getStep1
    var step2 = state.getters.getStep2

    var form_data = {}

    Object.assign(form_data, {remarks: step1.remarks})
    Object.assign(form_data, {approver: step1.authorizer})
    Object.assign(form_data, {project: step1.project})

    if (step2.dateRequired) 
      Object.assign(form_data, {purchase_required_date: step2.dateRequired})

    Object.assign(form_data, {requisition_products: step2.selectedItems.table.data})

    this.$api.Requisition.post({
      form: form_data,
      success: (data) => {
        state.dispatch('common/toggleConfirmManual', 
        {
          title: 'Requisition',
          message: 'Requisition successfully created!',
          possitiveAction: () => {
            state.dispatch('common/toggleConfirmManual', false, {root:true})
            this.$router.replace(`/transaction/requisition/user/${state.getters.getProduct.prod_id}`)
          },
          possitiveActionText: 'Go to My Requisition',
        }
        ,{root:true})
      },
      fail: (error) => {
        try {
          state.dispatch('common/toggleSnackbar', `${error.response.data.error}`, {root:true})
        }
        catch {
          state.dispatch('common/toggleSnackbar', 'Requisition Creation Failed', {root:true})
        }
        
      }
    })
  },
}
