import { requisition_manage_id } from '@/store-schema/requisition_manage_id'

export const state = () => ({
  viewmodel: new requisition_manage_id().toPOJO()
})



export const getters = {
  getRequisition: state => {
    return state.viewmodel.requisition
  },
  getStockReqTable: state => {
    return state.viewmodel.sr_items.table
  },
  getPurchaseReqTable: state => {
    return state.viewmodel.pr_items.table
  },
  getCheckoutsTable: state => {
    return state.viewmodel.ck_items.table
  }
}




export const mutations = {
  reset (state) {
    Object.assign(state.viewmodel, new requisition_manage_id().toPOJO())
  },
  setRequisition (state, data) {
    state.viewmodel.requisition = data
  },
  fillStockReqTable (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.sr_items.table, data)
  },
  fillPurchaseReqTable (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.pr_items.table, data)
  },
  fillCheckoutsTable (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.ck_items.table, data)
  },
  // MODAL ACTIONS
  toggleModal (state, key) {
    state.viewmodel.modals[key] = !state.viewmodel.modals[key]
  }
}



export const actions = {
  reset (state) {
    state.commit('reset')
  },
  async fetchRequisition(state, id) {
    return await this.$api.Requisition.get({
        slug: id,
        success: (data) => {
          state.commit('setRequisition', data)
        },
        fail: (error) => {
          tbl.loading = false
          tbl.data = []
        }
    })
  },
  async populateStockReqTable(state) {
    let tbl = state.getters.getStockReqTable

    return await this.$api.Requisition.listStocksRequisitionItems({
        slug: state.getters.getRequisition.id,
        params: this.$helpers.vuetifyTable.getParamsVuetify(tbl),
        success: (data) => {
          state.commit('fillStockReqTable', data)
        },
        fail: (error) => {
          tbl.loading = false
          tbl.data = []
        }
    })
  },
  async populatePurchaseReqTable(state) {
    let tbl = state.getters.getPurchaseReqTable

    return await this.$api.Requisition.listPurchaseRequestItems({
        slug: state.getters.getRequisition.id,
        params: this.$helpers.vuetifyTable.getParamsVuetify(tbl),
        success: (data) => {
          state.commit('fillPurchaseReqTable', data)
        },
        fail: (error) => {
          tbl.loading = false
          tbl.data = []
        }
    })
  },
  async populateCheckoutsTable(state) {
    let tbl = state.getters.getCheckoutsTable

    return await this.$api.Requisition.listCheckoutsItems({
        slug: state.getters.getRequisition.id,
        params: this.$helpers.vuetifyTable.getParamsVuetify(tbl),
        success: (data) => {
          state.commit('fillCheckoutsTable', data)
        },
        fail: (error) => {
          tbl.loading = false
          tbl.data = []
        }
    })
  }
}