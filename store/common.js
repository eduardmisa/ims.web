export const state = () => ({
  viewmodel: 
  {
    snackbar:
    {
      state: false,
      message:'',

      // SETTINGS
      mode: 'multi-line',
      x: null,
      timeout: 3000,
      y: 'top',
      // color: 'black',
    },
    loader: 
    {
      loader_manual: false
    },
    confirm_manual:
    {
      state: false,
      title: '',
      message: '',
      possitiveAction: null,
      possitiveActionText: null,
      negativeAction: null,
      negativeActionText: null
    }
  }
})

export const getters = {

}

export const mutations = {
  toggleSnackbar(state, message) {
    state.viewmodel.snackbar.message = message
    state.viewmodel.snackbar.state =  !state.viewmodel.snackbar.state
  },
  toggleLoader(state, key) {
    state.viewmodel.loader[key] = !state.viewmodel.loader[key]
  },
  toggleConfirmManual(state, obj) {

    if (obj === false) {
      state.viewmodel.confirm_manual.state = false
    }
    else {

      Object.assign(state.viewmodel.confirm_manual, {
        state: false,
        title: '',
        message: '',
        possitiveAction: null,
        possitiveActionText: null,
        negativeAction: null,
        negativeActionText: null
      })
  
      state.viewmodel.confirm_manual.state = true
  
      Object.assign(state.viewmodel.confirm_manual, obj)
    }
  }
}

export const actions = {
  toggleSnackbar(state, message) {
    state.commit('toggleConfirmManual', {
      title: 'IMS',
      message: message,
      possitiveActionText: 'Ok',
    })
  },
  toggleLoader(state, key) {
    state.commit('toggleLoader', key)
  },
  toggleConfirmManual (state, obj) {
    state.commit('toggleConfirmManual', obj)
  }
}
