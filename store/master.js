import objectHelper  from '@/helpers/object_helper.js'

const { Table } = require('../classes/table')


export const state = () => ({
  viewmodel: 
  {
    module_name: null,
    module: null,
    modals: false,
    index:
    {
      table: new Table().toPOJO()
    },
    isArchive: false,
    crud_form: {},
    crud_form_error: {},
  }
})



export const getters = {
  getViewmodel: state => {
    return state.viewmodel
  },
  getModuleName: state => {
    return state.viewmodel.module_name
  },
  getModule: state => {
    return state.viewmodel.module
  },
  getTable: state => {
    return state.viewmodel.index.table
  },
  getForm: state => {
    return state.viewmodel.crud_form
  },
  getFormError: state => {
    return state.viewmodel.crud_form_error
  },
  getModals: state => {
    return state.viewmodel.modals
  },
  getArchive: state => {
    return state.viewmodel.isArchive
  }
}



export const mutations = {
  resetAll (state) {
    Object.assign(state.viewmodel,
    {
      module_name: null,
      module: null,
      modals: false,
      index:
      {
        table: new Table().toPOJO()
      },
      isArchive: false,
      crud_form: {},
      crud_form_error: {},
    })
  },
  resetForm (state) {
    state.viewmodel.crud_form = {}
    state.viewmodel.crud_form_error = {}
  },
  setHeaders (state, headers) {
    state.viewmodel.index.table.headers = headers
  },
  setModuleName(state, name){
    state.viewmodel.module_name = name
  },
  setModule (state, moduleName) {
    state.viewmodel.module = moduleName
  },
  fillTable (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.index.table, data)
  },
  setField (state, changes) {
    Object.assign(state.viewmodel.crud_form, changes)
  },
  setIsArchive (state, isArchive) {
    state.viewmodel.isArchive = isArchive
  },
  toggleModal(state) {
    state.viewmodel.modals = !state.viewmodel.modals
  },

  // HELPER METHODS
  addFilter(state, objParam) {
    state.viewmodel.module_name = objParam.name
    state.viewmodel.index.table.additionalFilters.push(objParam)
  }
}



export const actions = {
  reset (state, formOnly) {
    if (!formOnly) {
      state.commit('resetAll')
    }
    else {
      state.commit('resetForm')
    }
  },
  setModuleName (state, name){
    state.commit('setModuleName', name)
  },
  setModule (state, moduleName) {
    state.commit('setModule', moduleName)
  },
  setHeaders (state, headers) {
    state.commit('setHeaders', headers)
  },
  list (state) {
    if (process.server) 
      return

    var tbl = state.getters.getTable

    return this.$api[state.getters.getModule].paginate({
        params: this.$helpers.vuetifyTable.getParamsVuetify(state.getters.getTable),
        success: (data) => {
          state.commit('fillTable', data)
        },
        fail: (error) => {
          console.log('Master error [list] @'+ state.getters.getModule)
          tbl.loading = false
          tbl.data = []
        }
    })
  },
  getItem (state, objParam) {

    // objParam.id
    // objParam.callback
    // objParam.errorCallback

    return this.$api[state.getters.getModule].get({
      slug: objParam.id,
      success: (data) => {
          state.commit('setField', data)
          objParam.callback()
        },
      fail: (error) => {
          console.log('Master error [getItem] @'+ state.getters.getModule)
          objParam.errorCallback()
        }
      })
  },
  submit (state) {

    var formError = state.getters.getFormError
    var form = state.getters.getForm
    var isArchive = state.getters.getArchive

    if (!isArchive) {
      if (!form.id || form.id === 0) {

        return this.$api[state.getters.getModule].post({
          form: form,
          success: (data) => {
              state.commit('toggleModal')
              state.dispatch('reset', true)
              state.dispatch('list')
              
              state.dispatch('common/toggleSnackbar', state.getters.getModuleName + ' Successfully Added!', {root:true})
            },
          fail: (error) => {
              if (error && error.response && error.response.data) {
                var errorMessageBody = error.response.data
                if (errorMessageBody) {
                  for (var field in errorMessageBody) {
                    if (errorMessageBody[field] === Array) {
                      errorMessageBody[field] = errorMessageBody[field].join(`<br/>`)
                    }
                  }
                  formError = objectHelper.merge(formError, errorMessageBody)
                }
              }
          
              state.dispatch('common/toggleSnackbar','Failed to add ' + state.getters.getModuleName + '!' , {root:true})
            }
        })
      }
      else {

        return this.$api[state.getters.getModule].patch({
          slug: form.id,
          form: form,
          success: (data) => {
              state.commit('toggleModal')
              state.dispatch('reset', true)
              state.dispatch('list')
              
              state.dispatch('common/toggleSnackbar', state.getters.getModuleName + ' Successfuly Updated!', {root:true})
            },
          fail: (error) => {
              if (error && error.response && error.response.data) {
                var errorMessageBody = error.response.data
                if (errorMessageBody) {
                  for (var field in errorMessageBody) {
                    if (errorMessageBody[field] === Array) {
                      errorMessageBody[field] = errorMessageBody[field].join(`<br/>`)
                    }
                  }
                  formError = objectHelper.merge(formError, errorMessageBody)
                }
              }
          
              state.dispatch('common/toggleSnackbar','Failed to update ' + state.getters.getModuleName + '!', {root:true})
            }
        })
      }
    }
    else {
      // ARCHIVE

      return this.$api[state.getters.getModule].delete({
        slug: form.id,
        success: (data) => {
            state.commit('toggleModal')
            state.dispatch('reset', true)
            state.dispatch('list')
            
            state.dispatch('common/toggleSnackbar', state.getters.getModuleName + ' Successfully Archived!', {root:true})
          },
        fail: (error) => {
            if (error && error.response && error.response.data) {
              var errorMessageBody = error.response.data
              if (errorMessageBody) {
                for (var field in errorMessageBody) {
                  if (errorMessageBody[field] === Array) {
                    errorMessageBody[field] = errorMessageBody[field].join(`<br/>`)
                  }
                }
                formError = objectHelper.merge(formError, errorMessageBody)
              }
            }
        
            state.dispatch('common/toggleSnackbar', 'Failed to archive! ' + state.getters.getModuleName + ' is used in other transactions.', {root:true})
          }
      })
    }
  },
  showModal (state, objParam) {

    var id = objParam.id
    var archive = objParam.isArchive

    state.dispatch('reset', true)

    if (!id) {
      state.commit('setIsArchive', false)
      state.commit('toggleModal')
    }
    else {
      state.commit('setIsArchive', archive)
      state.dispatch('getItem',
      {
        id: id,
        callback: () => {
          state.commit('toggleModal')
        },
        errorCallback: () => {
          state.dispatch('common/toggleSnackbar', 'Failed to fetch Data', {root:true})
        },
      })
    }
  },

  // HELPER METHODS
  addFilter(state, objParam){
    state.commit('addFilter', objParam)
    state.dispatch('list')
  }
}
