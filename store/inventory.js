import objectHelper  from '@/helpers/object_helper.js'
import { inventory } from '@/store-schema/inventory'


export const state = () => ({
  viewmodel: new inventory().toPOJO()
})



export const getters = {
  getViewmodel: state => {
    return state.viewmodel
  },
  getTable: state => {
    return state.viewmodel.index.table
  },
  getForm: state => {
    return state.viewmodel.crud_form
  },
  getFormError: state => {
    return state.viewmodel.crud_form_error
  },
  getModals: state => {
    return state.viewmodel.modals
  },
  getArchive: state => {
    return state.viewmodel.isArchive
  },

  getIndexTableFilter: state => {
    return state.viewmodel.index.filter
  },
}



export const mutations = {
  resetAll (state) {
    Object.assign(state.viewmodel, new inventory().toPOJO())
  },
  resetForm (state) {
    state.viewmodel.isArchive = false
    state.viewmodel.crud_form = {}
    state.viewmodel.crud_form_error = {}
  },
  setHeaders (state, headers) {
    state.viewmodel.index.table.headers = headers
  },
  setFormSchema (state, schema) {
    state.viewmodel.crud_form_schema = schema
  },
  fillTable (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.index.table, data)
  },
  setField (state, changes) {
    Object.assign(state.viewmodel.crud_form, changes)
  },
  setIsArchive (state, isArchive) {
    state.viewmodel.isArchive = isArchive
  },
  toggleModal(state) {
    state.viewmodel.modals = !state.viewmodel.modals
  },

  // HELPER METHODS
  addFilter(state, objParam) {
    state.viewmodel.index.table.additionalFilters.push(objParam)
  },

  // MODAL
  toggleCustomModal(state, key){
    state.viewmodel.custom_modals[key] = !state.viewmodel.custom_modals[key]
  }
}



export const actions = {
  reset (state, formOnly) {
    if (!formOnly) {
      state.commit('resetAll')
    }
    else {
      state.commit('resetForm')
    }
  },
  setHeaders (state, headers) {
    state.commit('setHeaders', headers)
  },
  setFormSchema (state, schema) {
    state.commit('setFormSchema', schema)
  },
  list (state) {
    if (process.server) 
      return

    var tbl = state.getters.getTable

    let params = this.$helpers.vuetifyTable.getParamsVuetify(tbl)

   // Filter
   var tblFilter = state.getters.getIndexTableFilter

   if (tblFilter.location.toggle) 
     params.push({key: 'location', value: tblFilter.location.selectedItem })

   if (tblFilter.brand.toggle) 
     params.push({key: 'brand', value: tblFilter.brand.selectedItem })

   if (tblFilter.category.toggle) 
     params.push({key: 'category', value: tblFilter.category.selectedItem })

    return this.$api.Inventory.paginate({
        params,
        success: (data) => {
          state.commit('fillTable', data)
        },
        fail: (error) => {
          console.log('Inventory error [list]')
          tbl.loading = false
          tbl.data = []
        }
    })
  },
  getItem (state, objParam) {
    // objParam.id
    // objParam.callback
    // objParam.errorCallback

    return this.$api.Product.get({
      slug: objParam.id,
      success: (data) => {
          state.commit('setField', data)
          objParam.callback()
        },
      fail: (error) => {
          console.log('Inventory error [getItem] ')
          objParam.errorCallback()
        }
      })
  },
  submit (state) {

    var formError = state.getters.getFormError
    var form = state.getters.getForm
    var isArchive = state.getters.getArchive

    if (!isArchive) {
      if (!form.id || form.id === 0) {

        return this.$api.Product.post({
          form: form,
          success: (data) => {
              state.commit('toggleModal')
              state.dispatch('reset', true)
              state.dispatch('list')
              
              state.dispatch('common/toggleSnackbar', 'Product Successfully Added!', {root:true})
            },
          fail: (error) => {
              if (error && error.response && error.response.data) {
                var errorMessageBody = error.response.data
                if (errorMessageBody) {
                  for (var field in errorMessageBody) {
                    if (errorMessageBody[field] === Array) {
                      errorMessageBody[field] = errorMessageBody[field].join(`<br/>`)
                    }
                  }
                  formError = objectHelper.merge(formError, errorMessageBody)
                }
              }
          
              state.dispatch('common/toggleSnackbar','Failed to add Product !' , {root:true})
            }
        })
      }
      else {

        return this.$api.Product.patch({
          slug: form.id,
          form: form,
          success: (data) => {
              state.commit('toggleModal')
              state.dispatch('reset', true)
              state.dispatch('list')
              
              state.dispatch('common/toggleSnackbar', 'Product Successfuly Updated!', {root:true})
            },
          fail: (error) => {
              if (error && error.response && error.response.data) {
                var errorMessageBody = error.response.data
                if (errorMessageBody) {
                  for (var field in errorMessageBody) {
                    if (errorMessageBody[field] === Array) {
                      errorMessageBody[field] = errorMessageBody[field].join(`<br/>`)
                    }
                  }
                  formError = objectHelper.merge(formError, errorMessageBody)
                }
              }
          
              state.dispatch('common/toggleSnackbar','Failed to update Product !', {root:true})
            }
        })
      }
    }
    else {
      // ARCHIVE

      return this.$api.Product.delete({
        slug: form.id,
        success: (data) => {
            state.commit('toggleModal')
            state.dispatch('reset', true)
            state.dispatch('list')
            
            state.dispatch('common/toggleSnackbar', 'Product Successfully Archived!', {root:true})
          },
        fail: (error) => {
            if (error && error.response && error.response.data) {
              var errorMessageBody = error.response.data
              if (errorMessageBody) {
                for (var field in errorMessageBody) {
                  if (errorMessageBody[field] === Array) {
                    errorMessageBody[field] = errorMessageBody[field].join(`<br/>`)
                  }
                }
                formError = objectHelper.merge(formError, errorMessageBody)
              }
            }
        
            state.dispatch('common/toggleSnackbar', 'Failed to archive! Product is used in other transactions.', {root:true})
          }
      })
    }
  },
  showModal (state, objParam) {
    var id = objParam.id
    var archive = objParam.isArchive

    state.dispatch('reset', true)

    if (!id) {
      state.commit('setIsArchive', false)
      state.commit('toggleModal')
    }
    else {
      state.commit('setIsArchive', archive)
      state.dispatch('getItem',
      {
        id: id,
        callback: () => {
          state.commit('toggleModal')
        },
        errorCallback: () => {
          state.dispatch('common/toggleSnackbar', 'Failed to fetch Data', {root:true})
        },
      })
    }
  },

  // HELPER METHODS
  addFilter(state, objParam){
    state.commit('addFilter', objParam)
    state.dispatch('list')
  }
}
