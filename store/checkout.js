import objectHelper  from '@/helpers/object_helper.js'
import { checkout } from '@/store-schema/checkout'

export const state = () => ({
  viewmodel: new checkout().toPOJO()
})


export const getters = {
  getRequisition: state => {
    return state.viewmodel.requisition
  },
  getStep1: state => {
    return state.viewmodel.step1
  },
  getStep2: state => {
    return state.viewmodel.step2
  },
  getRequsitionProductsTable: state => {
    return state.viewmodel.step2.requisition_products.table
  }
}



export const mutations = {
  reset (state) {
    Object.assign(state.viewmodel, new checkout().toPOJO())
  },
  setRequisition (state, data) {
    state.viewmodel.requisition = data
  },
  fillRequsitionProductsTable (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.step2.requisition_products.table, data)
  },
  step1_FilteredProjectLocations (state, data) {
    state.viewmodel.step1.filteredProjectLocations = []
    data.forEach(function(item) {
      state.viewmodel.step1.filteredProjectLocations.push(item)
    })
  },

  step2_SelectionSerialNumbers (state, data) {
    state.viewmodel.step2.serialized_items_list = []
    data.forEach(function(item) {
      item.selected = false
      state.viewmodel.step2.serialized_items_list.push(item)
    })
  },
  step2_SetProductForEdit (state, obj) {
    Object.assign(state.viewmodel.step2.selected_requisition_product, obj)
  },
  step2_AddSerialsToProduct (state) {

    var acquisitions_items = []

    var newCount = 0
    var newUsed = 0

    var existing = state.viewmodel.step2.requisition_products.table.data.find(exItem => exItem.product_no == state.viewmodel.step2.selected_requisition_product.product_no)

    if (existing) {


      state.viewmodel.step2.serialized_items_list.forEach((item) => {

        if (item.selected) {

          var obj = {}
  
          obj.quantity = 1
          obj.acquisition_item = item.acqId
          obj.requisition_product = existing.requisition_product_id
  
  
          //READ ONLY
          obj.serial_number = item.serial_number
          obj.selected = true
          obj.item_condition = item.item_condition
  
          acquisitions_items.push(obj)
  
          if (item.item_condition === 'New') 
            newCount += 1
          
          if (item.item_condition === 'Used') 
            newUsed += 1
        }
  
      })





      existing.qty_cn = newCount
      existing.qty_cu = newUsed

      existing.qty_ct = newCount + newUsed

      existing.acquisitions_items = []

      existing.acquisitions_items = acquisitions_items

    }
  },
  step2_WarehouseConsumablecount (state, data) {
    state.viewmodel.step2.consumable_items_list = []
    data.forEach(function(item) {
      state.viewmodel.step2.consumable_items_list.push(item)
    })
  },
  step2_AddConsumableToProduct (state) {

    var acquisitions_items = []

    var newCount = 0
    var newUsed = 0

    var existing = state.viewmodel.step2.requisition_products.table.data.find(exItem => exItem.product_no == state.viewmodel.step2.selected_requisition_product.product_no)

    if (existing) {

      state.viewmodel.step2.consumable_items_list.forEach((item) => {

        var obj = {}
  
        obj.acquisition_item = item.acqId
        obj.requisition_product = existing.requisition_product_id
        obj.acquisition_item = item.acqId
  
  
        if (item.item_condition === 'New') {
          obj.quantity = item.cn
          newCount += Number(obj.quantity ? obj.quantity : 0 )
        }
  
        if (item.item_condition === 'Used') {
          obj.quantity = item.cu
          newUsed += Number(obj.quantity ? obj.quantity : 0 )
        }
  
        acquisitions_items.push(obj)
      })

      existing.qty_cn = newCount
      existing.qty_cu = newUsed

      existing.qty_ct = newCount + newUsed

      existing.acquisitions_items = []

      existing.acquisitions_items = acquisitions_items

    }
  },

  // MODAL ACTIONS
  toggleModal (state, key) {
    state.viewmodel.modals[key] = !state.viewmodel.modals[key]
  }
}



export const actions = {
  reset (state) {
    state.commit('reset')
  },
  toggleModal (state, key) {
    state.commit('toggleModal', key)
  },
  async fetchRequisition(state, id) {
    return await this.$api.Requisition.get({
        slug: id,
        success: (data) => {
          state.commit('setRequisition', data)
        },
        fail: (error) => {

        }
    })
  },
  async populateRequsitionProductsTable (state) {
    let tbl = state.getters.getRequsitionProductsTable

    return await this.$api.Requisition.listStocksRequisitionItems({
        slug: state.getters.getRequisition.id,
        params: this.$helpers.vuetifyTable.getParamsVuetify(tbl),
        success: (data) => {
          state.commit('fillRequsitionProductsTable', data)
        },
        fail: (error) => {
          tbl.loading = false
          tbl.data = []
        }
    })
  },
  // STEP 1
  step1_FilteredProjectLocations (state) {

    var step1 = state.getters.getStep1

    state.dispatch('maintenance/populateProjectLocations', {
      params: [{key: 'project', value: step1.project}]
    }, {root: true})
  },
  // STEP 2
  async step2_SelectionSerialNumbers (state, callback) {

    var step1 = state.getters.getStep1
    var step2 = state.getters.getStep2

    await apiMasters.warehouse.get(step1.location_from + '/serialized-stocks/', [{key: 'productId', value: step2.selected_requisition_product.target_product_id }], 
      (data) => {
        state.commit('step2_SelectionSerialNumbers', data)
        callback()
      },
      (error) => {
        console.log('Checkout error [list]')
        tbl.loading = false
        tbl.data = []
      })
  },
  step2_AddSerialsToProduct (state) {
    state.commit('step2_AddSerialsToProduct')
  },
  async step2_WarehouseConsumablecount (state, callback) {

    var step1 = state.getters.getStep1
    var step2 = state.getters.getStep2

    await apiMasters.warehouse.get(step1.location_from + '/consumable-stocks/', [{key: 'productId', value: step2.selected_requisition_product.target_product_id }], 
      (data) => {
        debugger
        state.commit('step2_WarehouseConsumablecount', data)
        callback()
      },
      (error) => {
        console.log('Checkout error [list]')
        tbl.loading = false
        tbl.data = []
      })
  },
  step2_AddConsumableToProduct (state) {
    state.commit('step2_AddConsumableToProduct')
  },


  step2_SetProductForEdit (state, obj) {
    state.commit('step2_SetProductForEdit', obj)
  },

  SUBMIT(state, validatedBy) {

    var requisitionHeader = state.getters.getRequsitionHeader
    var step1 = state.getters.getStep1
    var step2 = state.getters.getStep2

    var form = {}

    form.remarks = step2.remarks
    // form.checkeout_on = ''
    form.checkeout_by = validatedBy
    // form.issued_by_id = ''
    form.location = step1.location
    form.location_from = step1.location_from
    form.requisition = requisitionHeader.id
    // form.status_id = ''

    form.ck_no = 'temp'

    form.requisition_products = []

    step2.requisition_products.table.data.forEach((item) => {

      if ('acquisitions_items' in item) {
        
        var requisition_product = {}

        requisition_product.id = item.requisition_product_id
        requisition_product.requisition = item.parent_requisition_id
        requisition_product.product = item.target_product_id
        requisition_product.acquisitions_items = item.acquisitions_items
  
        form.requisition_products.push(requisition_product)
      }
    })

    apiMasters.checkout.post(form, null, 
    (data) => {
      state.dispatch('toggleModal', 'step3Validator')
      state.dispatch('toggleModal', 'success')
    },
    (error) => {
      state.dispatch('common/toggleSnackbar', 'Checkout failed to be Submitted', {root:true})
    })
  }
}
