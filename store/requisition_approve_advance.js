import objectHelper  from '@/helpers/object_helper.js'
import { requisition_approve_advance } from '@/store-schema/requisition_approve_advance'

export const state = () => ({
  viewmodel: new requisition_approve_advance().toPOJO()
})



export const getters = {
  getRequisition: state => {
    return state.viewmodel.requisition
  },
  getStockReqTable: state => {
    return state.viewmodel.sr_items.table
  },
  getPurchaseReqTable: state => {
    return state.viewmodel.pr_items.table
  },
  getStockReqSelected: state => {
    return state.viewmodel.selected_sr_items
  },
  getPurchaseReqSelected: state => {
    return state.viewmodel.selected_pr_items
  }
}



export const mutations = {
  reset (state) {
    Object.assign(state.viewmodel, new requisition_approve_advance().toPOJO())
  },
  setRequisition (state, data) {
    state.viewmodel.requisition = data
  },
  fillStockReqTable (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.sr_items.table, data)
  },
  fillPurchaseReqTable (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.pr_items.table, data)
  },
  confirm (state) {

    state.viewmodel.sr_items.table.data = []
    state.viewmodel.pr_items.table.data = []

    state.viewmodel.selected_sr_items.forEach((item) => {
      state.viewmodel.sr_items.table.data.push(item)
    })

    state.viewmodel.selected_pr_items.forEach((item) => {
      state.viewmodel.pr_items.table.data.push(item)
    })

    state.viewmodel.selected_sr_items = []
    state.viewmodel.selected_pr_items = []
  },
}



export const actions = {
  reset (state) {
    state.commit('reset')
  },
  async fetchRequisition(state, id) {
    return await this.$api.Requisition.get({
        slug: id,
        success: (data) => {
          state.commit('setRequisition', data)
        },
        fail: (error) => {

        }
    })
  },
  async populateStockReqTable(state) {
    let tbl = state.getters.getStockReqTable

    return await this.$api.Requisition.listStocksRequisitionItemsForApproval({
        slug: state.getters.getRequisition.id,
        params: this.$helpers.vuetifyTable.getParamsVuetify(tbl),
        success: (data) => {
          state.commit('fillStockReqTable', data)
        },
        fail: (error) => {
          tbl.loading = false
          tbl.data = []
        }
    })
  },
  async populatePurchaseReqTable(state) {
    let tbl = state.getters.getPurchaseReqTable

    return await this.$api.Requisition.listPurchaseRequestItemsForApproval({
        slug: state.getters.getRequisition.id,
        params: this.$helpers.vuetifyTable.getParamsVuetify(tbl),
        success: (data) => {
          state.commit('fillPurchaseReqTable', data)
        },
        fail: (error) => {
          tbl.loading = false
          tbl.data = []
        }
    })
  },
  confirm (state, isConfirm) {
    if (isConfirm) {
      state.commit('confirm')
    }
    else {
      state.dispatch('populateStockReqTable')
      state.dispatch('populatePurchaseReqTable')
    }
  },
  async confirmApprove (state) {

    // let form = 
    // {
    //   sr_approve: {
    //       remarks: 'sr_approve APPROVE REMARKS',
    //       requisition: state.getters.getRequisition.id,
    //       requisitions_approve_items:[
    //         {
    //             quantity: 5,
    //             remarks: '',
    //             requisition_product: 8
    //         }
    //       ]
    //   },
    //   pr_approve: {
    //       remarks: 'pr_approve APPROVE REMARKS',
    //       purchase_request: 1,
    //       purchase_requests_approve_items:[
    //         {
    //             quantity: 3,
    //             remarks: '',
    //             purchase_request_item: 1
    //         }
    //       ]
    //   }
    // }
    let requisition = state.getters.getRequisition
    let selectedSR = state.getters.getStockReqTable.data
    let selectedPR = state.getters.getPurchaseReqTable.data

    let form = {}

    if (selectedSR.length > 0) {
      let sr_approve = {}
      Object.assign(sr_approve, {remarks: 'System Approved'})
      Object.assign(sr_approve, {requisition: requisition.id})

      let requisitions_approve_items = []
      selectedSR.forEach((item) => {
        requisitions_approve_items.push({
          quantity: item.qty_approve,
          remarks: '',
          requisition_product: item.row_id,
        })
      })

      Object.assign(sr_approve, {requisitions_approve_items: requisitions_approve_items})

      Object.assign(form, {sr_approve: sr_approve})
    }

    if (selectedPR.length > 0) {
      let pr_approve = {}
      Object.assign(pr_approve, {remarks: 'System Approved'})

      let purchase_requests_approve_items = []
      selectedPR.forEach((item) => {

        Object.assign(pr_approve, {purchase_request: item.pr_header_id})

        purchase_requests_approve_items.push({
          quantity: item.qty_approve,
          remarks: '',
          purchase_request_item: item.row_id,
        })
      })

      Object.assign(pr_approve, {purchase_requests_approve_items: purchase_requests_approve_items})

      Object.assign(form, {pr_approve: pr_approve})
    }

    this.$api.Requisition.approve({
      slug: state.getters.getRequisition.id,
      form,
      success: (data) => {
        state.dispatch('common/toggleConfirmManual', 
        {
          title: 'Requisition Approve',
          message: 'Requisition successfully approved!',
          possitiveAction: () => {
            state.dispatch('common/toggleConfirmManual', false, {root:true})
            this.$router.replace(`/transaction/requisition/for-approval`)
          },
          possitiveActionText: 'Go to My Requisitions For Approval',
        }
        ,{root:true})
      },
      fail: (error) => {
        try {
          state.dispatch('common/toggleSnackbar', `${error.response.data.error}`, {root:true})
        }
        catch {
          state.dispatch('common/toggleSnackbar', 'Requisition Approve Failed', {root:true})
        }
      }
    })
  }
}
