import objectHelper  from '@/helpers/object_helper.js'
import { inventory_id } from '@/store-schema/inventory_id'

export const state = () => ({
  viewmodel: new inventory_id().toPOJO()
})



export const getters = {
  getProduct: state => {
    return state.viewmodel.product
  },
  getInStoreLocation: state => {
    return state.viewmodel.in_store.location.table
  },
  getInStoreLocationBatch: state => {
    return state.viewmodel.in_store.location_batch.table
  },
  getOut: state => {
    return state.viewmodel.out.table
  },
  getReturns: state => {
    return state.viewmodel.returns.table
  },
  getBatchHistory: state => {
    return state.viewmodel.batch_history.table
  }
}



export const mutations = {
  reset (state) {
    Object.assign(state.viewmodel, new inventory_id().toPOJO())
  },
  setProduct (state, product) {
    state.viewmodel.product = product
  },
  fillInStoreLocation (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.in_store.location.table, data)
  },
  fillInStoreLocationBatch (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.in_store.location_batch.table, data)
  },
  fillOut (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.out.table, data)
  },
  fillReturns (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.returns.table, data)
  },
  fillBatchHistory (state, data) {
    this.$helpers.vuetifyTable.setTableDataVuetify(state.viewmodel.batch_history.table, data)
  }
}



export const actions = {
  reset (state) {
    state.commit('reset')
  },
  async fetchProduct (state, id) {
    await this.$api.Inventory.get({
      slug: id,
      success: (data) => {  
        state.commit('setProduct', data)
      },
      fail: () => {
        
      }
    })
  },
  async listInStoreLocation (state) {
    await this.$api.Inventory.listInStoreLocation({
      slug: state.getters.getProduct.prod_id,
      params: this.$helpers.vuetifyTable.getParamsVuetify(state.getters.getInStoreLocation),
      success: (data) => {  
        state.commit('fillInStoreLocation', data)
      },
      fail: () => {
        
      }
    })
  },
  async listInStoreLocationBatch (state) {
    await this.$api.Inventory.listInStoreLocationBatch({
      slug: state.getters.getProduct.prod_id,
      params: this.$helpers.vuetifyTable.getParamsVuetify(state.getters.getInStoreLocationBatch),
      success: (data) => {
        state.commit('fillInStoreLocationBatch', data)
      },
      fail: () => {
        
      }
    })
  },
  async listOut (state) {
    await this.$api.Inventory.listOut({
      slug: state.getters.getProduct.prod_id,
      params: this.$helpers.vuetifyTable.getParamsVuetify(state.getters.getOut),
      success: (data) => {
        state.commit('fillOut', data)
      },
      fail: () => {
        
      }
    })
  },
  async listReturns (state) {
    await this.$api.Inventory.listReturns({
      slug: state.getters.getProduct.prod_id,
      params: this.$helpers.vuetifyTable.getParamsVuetify(state.getters.getReturns),
      success: (data) => {
        state.commit('fillReturns', data)
      },
      fail: () => {
        
      }
    })
  },
  async listBatchHistory (state) {
    await this.$api.Inventory.listBatchHistory({
      slug: state.getters.getProduct.prod_id,
      params: this.$helpers.vuetifyTable.getParamsVuetify(state.getters.getBatchHistory),
      success: (data) => {
        state.commit('fillBatchHistory', data)
      },
      fail: () => {
        
      }
    })
  }
}
