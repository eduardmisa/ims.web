const { Table } = require('../classes/table')

const tableHelper = require('../helpers/tableHelper').default

  export const state = () => ({
    tblUser: new Table([
        { readonly: false, icon: 'text_format', type: 'Number', editable: false, text: 'ID', value: 'id', align: 'left' } ,
        { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'First Name', value: 'first_name', align: 'left' } ,
        { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Last Name', value: 'last_name', align: 'left' } ,
        { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Gender', value: 'gender', align: 'left' } ,
        { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Email', value: 'email', align: 'left' } ,
        { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'I.P', value: 'ip_address', align: 'left' } ,
    ]).toPOJO(),
    tblProduct: new Table([
        { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'ID', value: 'id', align: 'left' } ,
        { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Product No', value: 'product_no', align: 'left' } ,
        { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Product Name', value: 'product_name', align: 'left' } ,
        { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Product Description', value: 'product_description', align: 'left' } ,
        { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Product Qty', value: 'product_qty', align: 'left' } ,
    ]).toPOJO()
  })

  export const getters = {
    getTable: state => {
      return state.tblUser
    }
  }
  
  export const mutations = {
    reset (state) {
        Object.assign(state, { 
          tblUser: new Table([
            { readonly: false, icon: 'text_format', type: 'Number', editable: false, text: 'ID', value: 'id', align: 'left' } ,
            { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'First Name', value: 'first_name', align: 'left' } ,
            { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Last Name', value: 'last_name', align: 'left' } ,
            { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Gender', value: 'gender', align: 'left' } ,
            { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Email', value: 'email', align: 'left' } ,
            { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'I.P', value: 'ip_address', align: 'left' } ,
        ]).toPOJO(),
        tblProduct: new Table([
            { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'ID', value: 'id', align: 'left' } ,
            { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Product No', value: 'product_no', align: 'left' } ,
            { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Product Name', value: 'product_name', align: 'left' } ,
            { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Product Description', value: 'product_description', align: 'left' } ,
            { readonly: false, icon: 'text_format', type: 'String', editable: false, text: 'Product Qty', value: 'product_qty', align: 'left' } ,
        ]).toPOJO()
      })
    },
    SETTER (state, payload) {
        Object.assign(state, payload)
    },

    populateUsers ({tblUser}, data) {
      tableHelper.setTableDataVuetify(tblUser, data)
    },
    populateProducts ({tblProduct}, data) {
      tableHelper.setTableDataVuetify(tblProduct, data)
    }
  }

  export const actions = {
    getUsers ({commit, getters}) {

      commit('axiosLoader/textModal', 'FETCHING USERS PARE 3 SECONDS TO', {root: true})

      return this.$api.user.paginate({
          params: tableHelper.getParamsVuetify(getters.getTable), 
          success: (data) => {
              commit('populateUsers', data)
          }
      })
    },
    getProducts ({commit, getters}) {
      return request.get({
          url: masters.products, 
          params: tableHelper.getParamsVuetify(getters.getTable),
          success: (data) => {
              commit('populateProducts', data)
          }
      })
  }
}