export const state = () => ({
  viewmodel: 
  {
    brands: [],
    categories: [],
    clients: [],
    currencies: [],
    itemconditions: [],
    news: [], // NO POPULTE YEST
    projects:[],
    project_locations: [],
    unitmaterials: [],
    warehouses: [],
    suppliers: [],
    
    users: [],
    roles: [],
    permissions: [],
    
    yearMonthFilterset: []
  }
})

export const getters = {
  getUsers: state =>{
    return state.viewmodel.users
  },
  getBrands: state =>{
    return state.viewmodel.brands
  },
  getCategories: state => {
    return state.viewmodel.categories
  },
  getClients: state => {
    return state.viewmodel.clients
  },
  getCurrencies: state => {
    return state.viewmodel.currencies
  },
  getItemConditions: state => {
    return state.viewmodel.itemconditions
  },
  getProjects: state => {
    return state.viewmodel.projects
  },
  getProjectLocations: state => {
    return state.viewmodel.project_locations
  },
  getUnitMaterials: state =>{
    return state.viewmodel.unitmaterials
  },
  getWarehouses: state => {
    return state.viewmodel.warehouses
  },
  getSuppliers: state => {
    return state.viewmodel.suppliers
  },
}

export const mutations = {
  fillUser (state, list) {  
    state.viewmodel.users = []
    list.forEach((item) => {
      state.viewmodel.users.push(item)
    });
  },
  fillProductType (state, list) {  
    state.viewmodel.producttypes = []
    list.forEach((item) => {
      state.viewmodel.producttypes.push(item)
    });
  },
  fillWarehouse (state, list) {  
    state.viewmodel.warehouses = []
    list.forEach((item) => {
      state.viewmodel.warehouses.push(item)
    });
  },
  fillSupplier (state, list) {  
    state.viewmodel.suppliers = []
    list.forEach((item) => {
      state.viewmodel.suppliers.push(item)
    });
  },
  fillItemCondition (state, list) {  
    state.viewmodel.itemconditions = []
    list.forEach((item) => {
      state.viewmodel.itemconditions.push(item)
    });
  },
  fillCurrency (state, list) { 
    state.viewmodel.currencies = [] 
    list.forEach((item) => {
      state.viewmodel.currencies.push(item)
    });
  },
  fillRole (state, list) {
    state.viewmodel.roles = []
    list.forEach((item)=>{
      state.viewmodel.roles.push(item)
    })
  },
  fillPermission (state, list) {
    state.viewmodel.permissions = []
    list.forEach((item)=>{
      state.viewmodel.permissions.push(item)
    })
  },
  fillBrand (state, list) {
    state.viewmodel.brands = []
    list.forEach((item)=>{
      state.viewmodel.brands.push(item)
    })
  },
  fillUnitMaterial (state, list) {
    state.viewmodel.unitmaterials = []
    list.forEach((item)=>{
      state.viewmodel.unitmaterials.push(item)
    })
  },
  fillCategory (state, list) {
    state.viewmodel.categories = []
    list.forEach((item)=>{
      state.viewmodel.categories.push(item)
    })
  },
  fillProject (state, list) {
    state.viewmodel.projects = []
    list.forEach((item)=>{
      state.viewmodel.projects.push(item)
    })
  },
  fillProjectLocation (state, list) {
    state.viewmodel.project_locations = []
    list.forEach((item)=>{
      state.viewmodel.project_locations.push(item)
    })
  },
  fillClient (state, list) {
    state.viewmodel.clients = []
    list.forEach((item)=>{
      state.viewmodel.clients.push(item)
    })
  },
  fillYearMonth(state, list) {
    state.viewmodel.yearMonthFilterset = []
    list.forEach((item) => {
      state.viewmodel.yearMonthFilterset.push(item)
    })
  },
}

export const actions = {
  async populateUsers (state, {slug, params}) {
    await this.$api.User.get({
      slug,
      params,
      success: (data) => {
        state.commit('fillUser', data.results)
      }
    })
  },
  async populateWarehouses (state, {slug, params}) {
    await this.$api.Warehouse.get({
      slug,
      params,
      success: (data) => {
        state.commit('fillWarehouse', data.results)
      }
    })
  },
  async populateSuppliers (state, {slug, params}) {
    await this.$api.Supplier.get({
      slug,
      params,
      success: (data) => {
        state.commit('fillSupplier', data.results)
      }
    })
  },
  async populateItemConditions (state, {slug, params}) {
    await this.$api.ItemCondition.get({
      slug,
      params,
      success: (data) => {
        state.commit('fillItemCondition', data.results)
      }
    })
  },
  async populateCurencies (state, {slug, params}) {
    await this.$api.Currency.get({
      slug,
      params,
      success: (data) => {
        state.commit('fillCurrency', data.results)
      }
    })
  },
  // async populateRoles (state, {slug, params}) {
  //   apiMasters.role.get(null, null, (data) => {
    // slug,
    // params,
  //     state.commit('fillRole', data.results)
  //     if ({slug, params})
  //       callback()
  //   })
  // },
  // async populatePermissions (state, {slug, params}) {
  //   apiMasters.permission.get(null, null, (data) => {
    // slug,
    // params,
  //     state.commit('fillPermission', data.results)
  //     if ({slug, params})
  //       callback()
  //   })
  // },
  async populateBrands (state, {slug, params}) {
    await this.$api.Brand.get({
      slug,
      params,
      success: (data) => {
        state.commit('fillBrand', data.results)
      }
    })
  },
  async populateUnitMaterials (state, {slug, params}) {
    await this.$api.UnitMaterial.get({
      slug,
      params,
      success: (data) => {
        state.commit('fillUnitMaterial', data.results)
      }
    })
  },
  async populateCategories (state, {slug, params}) {
    await this.$api.Category.get({
      slug,
      params,
      success: (data) => {
        state.commit('fillCategory', data.results)
      }
    })
  },
  async populateProjects (state, {slug, params}) {
    await this.$api.Project.get({
      slug,
      params,
      success: (data) => {
        state.commit('fillProject', data.results)
      }
    })
  },
  async populateProjectLocations (state, {slug, params}) {
    await this.$api.ProjectLocation.get({
      slug,
      params,
      success: (data) => {
        state.commit('fillProjectLocation', data.results)
      }
    })
  },
  async populateClients (state, {slug, params}) {
    await this.$api.Client.get({
      slug,
      params,
      success: (data) => {
        state.commit('fillClient', data.results)
      }
    })
  }
}
