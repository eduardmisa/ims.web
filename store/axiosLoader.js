const axios = require('axios')

export const state = () => ({
    toggle: false,
    message: ''
  })
  
  export const mutations = {
    showModal (state) {
        state.toggle = true
    },
    closeModal (state) {
        state.toggle = false
        state.message = ''
    },
    textModal (state, msg) {
        state.message = msg
    }
  }