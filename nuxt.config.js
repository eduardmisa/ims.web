const pkg = require('./package')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/app.styl',
    '~/assets/css/main.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify',
    '@/plugins/input-field',
    '@/plugins/table-template',
    '@/plugins/service',

    '@/plugins/helpers',
    { src: "~/plugins/vue2-google-maps", ssr: true },
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/moment',
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    proxy: false,// Can be also an object with default options
    // host: require('./nuxt.env.json').axios_frontend_host,
    // port: require('./nuxt.env.json').axios_frontend_port,
  },
  proxy: {
    // '/api': 'http://localhost:3000/api',
    // '/auth': 'http://localhost:3000/auth',
  },
  /*
  ** Build configuration
  */
  build: {
    transpile: ['vuetify/lib',/^vue2-google-maps($|\/)/],
    plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      stylus: {
        import: ['~assets/style/variables.styl']
      }
    },
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },
  router: {
    middleware: ['notAuthenticated', 'permissionAccess']
  },
  auth:{
    strategies: {
      local: {
        watchLoggedIn: true,
        endpoints: {
          login: {url: 'auth/login/', method: 'post', propertyName: 'token' },
          user: {url: 'auth/current-user/', method: 'get', propertyName: false },
          logout: true
        },
        tokenRequired: true,
        tokenType: 'Bearer'
      }
    },
    redirect: {
      home: '/',
      login: '/login',
      logout: '/',
    }
  }
}
