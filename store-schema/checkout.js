const { Table } = require('../classes/table')

export const checkout = class checkout {
    constructor () {
        this.modals = {
          step2Serialized: false,
          step2Consumable: false,
          step3Validator: false,
          success: false,
          help: false
        }
        this.requisition = {}
        this.step1 = {
          location_from: null,
          project: null,
          location: null,
          filteredProjectLocations: [],
        }
        this.step2 = {
          requisition_products: {
            table: new Table([
              { readonly: false, icon: 'text_format', type: 'String', text: 'Product Name', value: 'product_name', align: 'left' },
              { readonly: false, icon: 'text_format', type: 'String', text: 'Product No', value: 'product_no', align: 'left' },
              { readonly: false, icon: 'text_format', type: 'String', text: 'Brand', value: 'brand', align: 'left' },
              { readonly: false, icon: 'text_format', type: 'String', text: 'Category', value: 'category', align: 'left' },
              { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_RC', value: 'qty_rc', align: 'left' },
              { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_CN', value: 'qty_cn', align: 'left' },
              { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_CU', value: 'qty_cu', align: 'left' },
              { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_CT', value: 'qty_ct', align: 'left' },
  
              { readonly: false, icon: 'text_format', type: null, text: 'Actions', value: null, align: 'center' }
            ]).toPOJO()
          },
          selected_requisition_product: {},
          serialized_items_list: [],
          consumable_items_list: [],
          remarks: null,
        }
        this.help = {
          abbreviation: {
            items: [ 
              { value: "Qty_RC ", description: "Quantity Remaining for Checkout" }, 
              { value: "Qty_CN  ", description: "Quantity Checkout New" }, 
              { value: "Qty_CU  ", description: "Quantity Checkout Used" }, 
              { value: "Qty_CT  ", description: "Quantity Checkout Total" } 
            ]
          }
        }
    }

    toPOJO () {
      return {
        modals: this.modals,
        requsition: this.requsition,
        step1: this.step1,
        step2: this.step2,
        help: this.help,
      }
    }
}