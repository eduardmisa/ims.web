const { Table } = require('../classes/table')

export const requisition_for_approval = class requisition_for_approval {
    constructor () {
        this.index = {
          table: new Table([
            { readonly: false, icon: 'text_format', type: 'String', text: 'Stock Request No.', value: 'requisition_no', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'Object', select_items: null, display_name: 'username', select_items_value: 'id', text: 'Requestor', value: 'requestor', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Status', value: 'status', align: 'left' },

            { readonly: false, icon: null, type: null, text: 'Actions', value: null, align: 'left', width: '50px;', sortable: false }
          ]).toPOJO(),
        }
    }

    toPOJO () {
      return {
        index: this.index
      }
    }
}