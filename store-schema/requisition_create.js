const { Table } = require('../classes/table')

export const requisition_create = class requisition_create {
    constructor () {
        this.modals = {
            help: false
        }
        this.help = {
            abbreviation: {
                items: [ 
                    { value: "Qty_IS ", description: "Quantity In Store" }, 
                    { value: "Qty_PRI  ", description: "Quantity Pending Request Items" }, 
                    { value: "Qty_VB  ", description: "Quantity Virtual Balance (Qty_IS - Qty_PR)" } 
                ]
            }
        }
        this.step1 = {
            authorizerObject: {},
            showAllProjects: true,
            client: null,
            project: null,
            projectDescription: null,
            authorizer: 0,
            remarks: "",
            showHiddenStocks: false,
            filteredProjects: []
        }
        this.step2 = {
            hasPR: false,
            selectionItems: {
              table: new Table([
                { readonly: false, icon: 'text_format', type: 'String', text: 'Product No.', value: 'productNo', align: 'left', sortable: false },
                { readonly: false, icon: 'text_format', type: 'String', text: 'Product Name', value: 'productName', align: 'left', sortable: false },
                { readonly: false, icon: 'text_format', type: 'String', text: 'Brand', value: 'productBrand', align: 'left', sortable: false },
                { readonly: false, icon: 'text_format', type: 'String', text: 'Category', value: 'productCategory', align: 'left', sortable: false },
                { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_VB', value: 'qty_vb', align: 'left', sortable: false },
                { readonly: false, icon: 'text_format', type: 'String', text: 'QTY_Add', value: 'qty_add', align: 'left', sortable: false, width: '90px' },
                { readonly: false, icon: 'text_format', type: 'String', text: 'Unit', value: 'productUnit', align: 'left', sortable: false },
                
                { readonly: false, icon: 'text_format', type: null, text: 'Actions', value: null, align: 'center', sortable: false }
              ]).toPOJO()
            },
            selectedItems: {
              table: new Table([
                { readonly: false, icon: 'text_format', type: 'String', text: 'Product No.', value: 'productNo', align: 'left', sortable: false },
                { readonly: false, icon: 'text_format', type: 'String', text: 'Product Name', value: 'productName', align: 'left', sortable: false },
                { readonly: false, icon: 'text_format', type: 'String', text: 'Brand', value: 'productBrand', align: 'left', sortable: false },
                { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_SR', value: 'qty_sr', align: 'left', sortable: false },
                { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_PR', value: 'qty_pr', align: 'left', sortable: false },
                { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_Minus', value: 'qty_minus', align: 'left', sortable: false, width: '90px' },
                { readonly: false, icon: 'text_format', type: 'String', text: 'Unit', value: 'productUnit', align: 'left', sortable: false },
                
                { readonly: false, icon: 'text_format', type: null, text: 'Actions', value: null, align: 'center', sortable: false }
              ]).toPOJO()
            },
            searchSelectionItems: "",
            dateRequired: null,
            help:
            {
              abbreviation:
              {
                items: [ 
                  { value: "Qty_IS ", description: "Quantity In Store" }, 
                  { value: "Qty_PRI  ", description: "Quantity Pending Request Items" }, 
                  { value: "Qty_VB  ", description: "Quantity Virtual Balance (Qty_IS - Qty_PR)" },
                  { value: "Qty_SR  ", description: "Quantity to Stock Request" } ,
                  { value: "Qty_PR  ", description: "Quantity to Purchase Request" }  
                ]
              }
            }
        }
        this.step3 = {

        }
        this.form = {
            requested_date: null,
            checkout_close: false,
            approve_as_urgent: false,
            remarks: null,
            requestor: null,
            approver: null,
            project: null,
            purchase_required_date: null,
            requisition_products:[
                {
                    quantity: null, // NULL
                    remarks: null,
                    product: null,
                    qty_sr: 0,
                    qty_pr: 0
                }
            ]
        }

    }

    toPOJO () {
      return {
        modals: this.modals,
        help: this.help,
        step1: this.step1,
        step2: this.step2,
        step3: this.step3,
        form: this.form
      }
    }
}