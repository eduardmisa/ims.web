const { Table } = require('../classes/table')

export const inventory = class inventory {
    constructor () {
      this.custom_modals = {
        product: false,
        filter: false,
        help: false,
      }
      this.modals = false,
      this.index = {
        table: new Table().toPOJO(),
        filter:
        {
          location:
          {
            toggle: false,
            selectedItem: 0,
          },
          brand:
          {
            toggle: false,
            selectedItem: 0,
          },
          category:
          {
            toggle: false,
            selectedItem: 0,
          }
        },
        help:
        {
          abbreviation:
          {
            items: [ 
              { value: "Qty_IS ", description: "Quantity In Store" }, 
              { value: "Qty_PRI  ", description: "Quantity Pending Request Items" }, 
              { value: "Qty_VB  ", description: "Quantity Virtual Balance (Qty_IS - Qty_PR)" } 
            ]
          }
        },
      }
      this.isArchive = false
      this.crud_form_schema = []
      this.crud_form = {}
      this.crud_form_error = {}
    }

    toPOJO () {
      return {
        custom_modals: this.custom_modals,
        modals: this.modals,
        index: this.index,
        isArchive: this.isArchive,
        crud_form_schema: this.crud_form_schema,
        crud_form: this.crud_form,
        crud_form_error: this.crud_form_error,
      }
    }
}