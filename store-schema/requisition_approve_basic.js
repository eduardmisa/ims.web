const { Table } = require('../classes/table')

export const requisition_approve_basic = class requisition_approve_basic {
    constructor () {
        this.requisition = {}
        this.sr_items = {
          table: new Table([
            { readonly: false, icon: 'text_format', type: 'String', text: 'Product No.', value: 'product_no', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Product Name', value: 'product_name', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Qty', value: 'qty_remaining', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Unit', value: 'product_unit', align: 'left' },
          ]).toPOJO(),
        }
        this.pr_items = {
          table: new Table([
            { readonly: false, icon: 'text_format', type: 'String', text: 'Product No.', value: 'product_no', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Product Name', value: 'product_name', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Qty', value: 'qty_remaining', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Unit', value: 'product_unit', align: 'left' },
          ]).toPOJO(),
        },
        this.selected_sr_items = []
        this.selected_pr_items = [],

        this.confirm = false
    }

    toPOJO () {
      return {
        requisition: this.requisition,
        sr_items: this.sr_items,
        pr_items: this.pr_items,
        selected_sr_items: this.selected_sr_items,
        selected_pr_items: this.selected_pr_items,
        confirm: this.confirm
      }
    }
}