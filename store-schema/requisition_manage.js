const { Table } = require('../classes/table')

export const requisition_manage = class requisition_manage {
    constructor () {
      this.index = {
        checkoutOpen: {
          table: new Table([
            { readonly: false, icon: 'date_range', type: 'Date', text: 'Requested On', value: 'requested_date', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Stock Request No.', value: 'requisition_no', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'Object', display_name: 'name', text: 'Project', value: 'project', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', display_name: 'description', text: 'Status', value: 'status', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Last Updated By', value: 'modifiedby', align: 'left' },
            { readonly: false, icon: 'date_range', type: 'Date', text: 'Last Updated On', value: 'modified', align: 'left' },
            // { readonly: false, icon: 'text_format', type: 'String', text: 'Locations', value: 'modifiedby', align: 'left' },

            { readonly: false, icon: null, type: null, text: 'Actions', value: null, align: 'left', width: '50px;', sortable: false }
          ]).toPOJO(),
        },
        checkoutClose: {
          table: new Table([
            { readonly: false, icon: 'date_range', type: 'Date', text: 'Requested On', value: 'requested_date', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Stock Request No.', value: 'requisition_no', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'Object', display_name: 'name', text: 'Project', value: 'project', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', display_name: 'description', text: 'Status', value: 'status', align: 'left' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Last Updated By', value: 'modifiedby', align: 'left' },
            { readonly: false, icon: 'date_range', type: 'Date', text: 'Last Updated On', value: 'modified', align: 'left' },
            // { readonly: false, icon: 'text_format', type: 'String', text: 'Locations', value: 'modifiedby', align: 'left' },

            { readonly: false, icon: null, type: null, text: 'Actions', value: null, align: 'left', width: '50px;', sortable: false }
          ]).toPOJO(),
        }
      }
    }

    toPOJO () {
      return {
        index: this.index,
      }
    }
}