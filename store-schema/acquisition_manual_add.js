const { Table } = require('../classes/table')

export const ManualAdd = class ManualAdd {
    constructor () {
        this.currentProduct = {}
        this.form = {
          acquired_date: null,
          batch_no: 1,
          po_no: null,
          inv_no: null,
          batch_price: null,
          warehouse: null,
          supplier: null,
          stocks: new Table([
              { readonly: false, icon: 'text_format', type: 'String', text: 'Serial Number', value: 'serial_number' },
              { readonly: false, icon: 'text_format', type: 'String', text: 'Type', value: 'item_condition' },
              { readonly: false, icon: 'text_format', type: 'String', text: 'Currency', value: 'currency' },
              { readonly: false, icon: 'text_format', type: 'String', text: 'Price', value: 'price' },
          ]).toPOJO(),
          // [
              // {
              //     serial_number: SERIAL_POSTMANE100,
              //     product: 1,
              //     acquisition_items: [
              //         {
              //             quantity: 1,
              //             price: 50,
              //             currency: 1,
              //             item_condition: 1,
              //             stock: null,
              //             acquisition: null
              //         }
              //     ]
              // }
          // ]
        },
        this.form_temp= {
            serials: [],
            item_condition: null,
            currency: null,
            total_price: 0,
            total_quantity: 0
        }
    }

    toPOJO () {
      return {
        currentProduct: this.currentProduct,
        form: this.form,
        form_temp: this.form_temp
      }
    }
}