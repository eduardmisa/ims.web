const { Table } = require('../classes/table')

export const requisition_manage_id = class requisition_manage_id {
    constructor () {
      this.modals = {
        help: false,
        resend: false
      },
      this.requisition = {}
      this.sr_items = {
        table: new Table([
          { readonly: false, icon: 'text_format', type: 'String', text: 'Product No.', value: 'product_no', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Product Name', value: 'product_name', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Brand', value: 'brand', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Category', value: 'category', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Status', value: 'req_status', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_SR', value: 'qty_sr', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_C', value: 'qty_c', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_RC', value: 'qty_rc', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Unit', value: 'unit', align: 'left' },
          // { readonly: false, icon: 'text_format', type: 'String', text: 'Locations', value: 'location', align: 'left' },
        ]).toPOJO(),
      }
      this.pr_items = {
        table: new Table([
          { readonly: false, icon: 'text_format', type: 'String', text: 'Product No.', value: 'product_no', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Product Name', value: 'product_name', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Brand', value: 'product_brand', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Category', value: 'product_category', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Status', value: 'pr_item_status', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_PR', value: 'qty_pr', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Unit', value: 'product_unit', align: 'left' },
        ]).toPOJO(),
      }
      this.ck_items = {
        table: new Table([
          { readonly: false, icon: 'text_format', type: 'String', text: 'Checkout No.', value: 'd_checkout_no', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Checkout On', value: 'd_checkout_on', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'From Location', value: 'd_checkout_from_location', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Issued By', value: 'd_checkout_issued_by', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Checkout By', value: 'd_checkout_checkout_by', align: 'left' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Remarks', value: 'd_checkout_remarks', align: 'left' },
          { readonly: false, icon: null, type: null, text: 'Actions', value: null, align: 'left', width: '50px;', sortable: false },
        ]).toPOJO(),
      }
      this.help = {
        abbreviation:
        {
          items: [ 
            { value: "Qty_SR ", description: "Quantity of Stock Request" },
            { value: "Qty_C  ", description: "Quantity Checkouts" }, 
            { value: "Qty_RC  ", description: "Quantity Remaining for Checkout" },
            { value: "Qty_PR ", description: "Quantity of Purchase Request" },
            { value: "Qty_CN ", description: "Quantity Checkout New" },
            { value: "Qty_CU ", description: "Quantity Checkout Used" },
            { value: "Qty_CT ", description: "Quantity Checkout Total" },
            { value: "Qty_ER ", description: "Quantity Checkout Error Return" }
          ]
        }
      }
    }

    toPOJO () {
      return {
        modals: this.modals,
        requisition: this.requisition,
        sr_items: this.sr_items,
        pr_items: this.pr_items,
        ck_items: this.ck_items,
        help: this.help,
      }
    }
}