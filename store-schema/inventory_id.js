const { Table } = require('../classes/table')

export const inventory_id = class inventory_id {
    constructor () {
      this.modals = {

      }
      this.product = {}
      this.in_store = {
        location: {
          table: new Table([
            { readonly: false, icon: 'text_format', type: 'String', text: 'Location', value: 'current_warehouse' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Qty New', value: 'qty_n' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Qty Used', value: 'qty_u' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Qty Total', value: 'qty_t' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Unit', value: 'unit_material' },
          ]).toPOJO()
        },
        location_batch: {
          table: new Table([
            { readonly: false, icon: 'text_format', type: 'String', text: 'Batch', value: 'batch' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Location', value: 'current_warehouse' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Currency', value: 'stock_currency' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Stock in date', value: 'acquired_date' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'PO No.', value: 'po_no' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'INV No.', value: 'inv_no' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Unit', value: 'unit_material' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Supplier', value: 'supplier' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_N', value: 'qty_n' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_U', value: 'qty_u' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_T', value: 'qty_t' },
            { readonly: false, icon: 'text_format', type: 'String', text: 'Total Price.', value: 'total_price' },
  
            { readonly: false, icon: 'text_format', type: null, text: 'Actions', value: null, align: 'left' }
          ]).toPOJO()
        }
      }
      this.out = {
        table: new Table([
          { readonly: false, icon: 'text_format', type: 'String', text: 'Deployment Date', value: 'deployment_date' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Location', value: 'location' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Project', value: 'project' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Requisition No', value: 'requisition_no' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Issued By', value: 'issued_by' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Checked out By', value: 'checkout_by' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Requestor Remarks', value: 'requestor_remarks' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Collector Remarks', value: 'remarks_collector' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_N', value: 'qty_n' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_U', value: 'qty_u' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_T', value: 'qty_t' },
        ]).toPOJO()
      }
      this.returns = {
        table: new Table([
          { readonly: false, icon: 'text_format', type: 'String', text: 'Requisition No.', value: 'requisition_no' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Return Date', value: 'return_date' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Warehouse', value: 'to_warehouse' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'From', value: 'from_project' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Return by', value: 'return_by_user' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_N', value: 'qty_n' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_U', value: 'qty_u' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Qty_T', value: 'qty_t' },
  
        ]).toPOJO()
      }
      this.batch_history = {
        table: new Table([
          { readonly: false, icon: 'text_format', type: 'String', text: 'batch', value: 'batch_no' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Stock in date', value: 'acquired_date' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Currency', value: 'currency_d' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'PO No.', value: 'po_no' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'INV No.', value: 'inv_no' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Supplier', value: 'supplier_d' },
          
          { readonly: false, icon: 'text_format', type: 'String', text: 'Total Price', value: 'total_price' },
          { readonly: false, icon: 'text_format', type: 'String', text: 'Qty', value: 'qty' },
        ]).toPOJO()
      }
      // chart: {
      //   table: new Table().toPOJO()
      // },
      // search_serial: {
      //   table: new Table().toPOJO()
      // }
    }

    toPOJO () {
      return {
        modals: this.modals,
        product: this.product,
        in_store: this.in_store,
        out: this.out, 
        returns: this.returns,
        batch_history: this.batch_history,
      }
    }
}