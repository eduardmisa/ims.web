// import configHelper from '@/helper/config_helper.js'

export default {
  getParamsVuetify(tbl) {
    // additionalFilters: [],
    // data: [], 
    // loading: false,
    // search: { field: '', value: '', fields: {} },
    // pagination:
    // {
    //   descending: false,
    //   page: 1,
    //   rowsPerPage: 5, // -1 for All
    //   sortBy: '',
    //   totalItems: ''
    // }

    var params = []

    tbl.loading = true

    // Paging

    if (tbl.pagination.rowsPerPage && tbl.pagination.rowsPerPage < 0 || tbl.pagination.rowsPerPage > 50) {
    //   tbl.pagination.rowsPerPage = configHelper.GetValue('max_page')
        // tbl.pagination.rowsPerPage = 50
    }

    params.push({key: 'page', value: tbl.pagination.page })
    params.push({key: 'page-size', value: tbl.pagination.rowsPerPage })

    // Sorting
    if (tbl.pagination && tbl.pagination.sortBy)
      params.push({key: 'sort-field', value: tbl.pagination.sortBy })

    if (tbl.pagination && typeof tbl.pagination.descending == 'boolean')
      params.push({key: 'sort-order', value: tbl.pagination.descending ? 'desc' : 'asc' })

    // Search text
    // params.push({key: tbl.search.field , value: tbl.search.value })
    params.push({key: 'search', value: tbl.search.value })

    // Check for additional filtering

    if ('additionalFilters' in tbl && tbl.additionalFilters.length > 0) {
      tbl.additionalFilters.forEach((item) => {
        params.push({key: item.key, value: item.value})
      })
    }

    return params
  },
  setTableDataVuetify(tbl, data) {
    
    if (data) {

      tbl.data = []

      data.results.forEach((item) => {
        tbl.data.push(item)
      });
  
      tbl.pagination.page = data.page
      tbl.total = parseInt(data.count)
      tbl.loading = false

      // Set header fields here:

      tbl.headers
      var flds = {}
  
      tbl.headers.forEach(items => {
          if(items.text != 'Actions'){
  
              var text = items.text
              var value = items.value
              
              if (items.type === 'Object'){
                Object.assign(flds, {
                  [String(text)]: items.value + '.' + items.display_name
                })
              } 
              else {
                Object.assign(flds, {
                  [String(text)]:value
                })
              }   
          }
      });

      tbl.search.fields = flds
    }
  },



  
  changeSort (column, table, callback) {
    if (table.pagination.sortBy === column) {
      table.pagination.descending = !table.pagination.descending
    } else {
      table.pagination.sortBy = column
      table.pagination.descending = false
    }

    callback()
  },
  
  // exportToPDF(table, name){
  //   var jsPDF = require('jspdf');
  //   require('jspdf-autotable'); 
  //   var columns = []
  //   var rows = []

  //   var arrayOfObjectType = []

  //   var tableHeaders = table.headers
  //   tableHeaders.forEach(items => {
  //     if(items.text != 'Actions'){
  //         var columnsData = { title: items.text , dataKey: items.value }
  //         columns.push(columnsData)

  //         if(items.type === 'Object')
  //           arrayOfObjectType.push(
  //             { 
  //               key: items.value, 
  //               value: items.display_name
  //             })
  //     }
  //   });

  //   var tableData = table.data
  //   tableData.forEach(items => {

  //       var newRow = Object.assign({}, items)

  //       for (var key in newRow) {

  //         if (newRow[key] && typeof newRow[key] === "object") {

  //           var foundKey = arrayOfObjectType.find(exKey => exKey.key == key)

  //           if (foundKey) 
  //             newRow[key] = newRow[key][foundKey.value]
  //         }
  //       }

  //       rows.push(newRow)
  //   });

  //   if (rows.length > 0){
  //     var doc = new jsPDF('p', 'pt');
  //     doc.text(name, 40, doc.autoTableEndPosY() + 30);
  //     doc.autoTable(columns, rows, {
  //       headStyles:{fillColor:85},
  //       margin: {top: 60}
  //     });
  //     doc.save(name+'.pdf');
  //   }

  // }
}