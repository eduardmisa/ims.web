import app_configs from '@/nuxt.env.json'

export default {
	GetValue (key) {
		return app_configs[key]
	}
}