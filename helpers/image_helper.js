export default {
    ImageUrlToBase64 (url, callback) {
        if (url) {

            var httpRequest = new XMLHttpRequest();
            httpRequest.onload = function() {
                var fileReader = new FileReader();
                    fileReader.onloadend = function() {
                        callback(fileReader.result);
                    }
                    fileReader.readAsDataURL(httpRequest.response);
            };
            httpRequest.open('GET', url);
            httpRequest.responseType = 'blob';
            httpRequest.send();

            // var outputFormat = url.slice(-3)

            // var img = new Image();
            // img.crossOrigin = 'Anonymous';
            // img.onload = function() {
            //     var canvas = document.createElement('CANVAS');
            //     var ctx = canvas.getContext('2d');
            //     var dataURL;
            //     canvas.height = this.height;
            //     canvas.width = this.width;
            //     ctx.drawImage(this, 0, 0);

            //     // TODO: MAKE THIS THREAD SEPARATE
            //     dataURL = canvas.toDataURL(outputFormat);

            //     canvas.

            //     callback(dataURL);
            //     canvas = null;
            // };
            // img.src = url;
        }
    }
  }