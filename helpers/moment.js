import moment from 'moment'


export function MomentHelper (date) {

  if (date) {

    var mdate = moment(date)

    if (mdate.isValid()) {

      return mdate.format('DD/MM/YYYY');
    }
  }

  return date
}