export default {
    IsPermitted (user, currentPath) {

      var isPermmitted = false

      if (user.permission_access && user.permission_access.length > 0) 
        for (var i = 0; i < user.permission_access.length; i++) {
          if (currentPath === user.permission_access[i].desc)  
            isPermmitted = true
        }
        
      return isPermmitted
    },
    willShowModule (user, currentPath) {

      var isPermmitted = false

      if (user.permission_access && user.permission_access.length > 0) 
        for (var i = 0; i < user.permission_access.length; i++) {
          if (currentPath === user.permission_access[i].path)  
            isPermmitted = true
        }
      return isPermmitted
    }
}