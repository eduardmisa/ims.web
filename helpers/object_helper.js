import { stateMerge } from "vue-object-merge"

function removeProp(obj, key) {
  for (var i in obj) {
    if (obj.hasOwnProperty(i)) {
      if (i == key) {
        obj[key] = undefined
      }
      else if (typeof obj[i] == 'object') {
        removeProp(obj[i], key)
      }
    }
  }
}


export default {
  merge (data, obj) {

    // DEEP MERGES Objects
    // This will Remove Vue's reactive properties set on data object

    var objectMerge = require('object-merge');

    var merged = objectMerge(data, obj)

    // recreates data object's reactive properties

    stateMerge(data, merged)

    return data
  },
  unmerge (data) {
    
    var tempObject = Object.assign({}, data)

    removeProp(tempObject, 'batch_no_error')
    removeProp(tempObject, 'acquired_date_error')
    removeProp(tempObject, 'po_no_error')
    removeProp(tempObject, 'inv_no_error')
    removeProp(tempObject, 'serial_number_error')
    removeProp(tempObject, 'product_error')
    removeProp(tempObject, 'price_error')
    removeProp(tempObject, 'total_quantity_error')
    removeProp(tempObject, 'warehouse_error')
    removeProp(tempObject, 'supplier_error')
    removeProp(tempObject, 'condition_error')
    removeProp(tempObject, 'currency_error')

    stateMerge(data, tempObject)

    return data
  }
}

